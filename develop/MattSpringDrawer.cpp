#include <qapplication.h>
#include <qwidget.h>
#include <qpoint.h>
#include <qpen.h>
#include <qpainter.h>
#include <math.h>
#include <iostream>

// This is a demo class for testing a spring-drawing algorithm.

class SpringDrawer : public QWidget{

public:
	SpringDrawer(QWidget* parent = 0, const char *name=0);
	
protected:
    virtual void mousePressEvent ( QMouseEvent * );
    virtual void mouseReleaseEvent ( QMouseEvent * );
    virtual void mouseMoveEvent( QMouseEvent *me );
    void paintEvent( QPaintEvent * );

private:
	QPoint p1;   //the two particles to draw the spring
	QPoint p2;   // between
	QPoint a;    // the bits the kinks are actually connected
	QPoint b;    // to, these are connected to the points by
				 // straight lines.
	int springCount = 10;	  //the number of kinks in the spring;
	float leadLength = 10;
	int h;		 // half the width of the spring at its rest length
    float y;      //length of 1/2 of a kink at rest length
    float lr;     //rest length
	float theta;  //guess what

	QPoint* selected;
	QPoint* nearest(const QPoint&);
	void drawSpring(QPainter& paint);
	void putPointOnLine(QPoint& p, float f);
	QPen dotPen;
};

SpringDrawer::SpringDrawer(QWidget* parent, const char *name):
	QWidget(parent,name),p1(50, 100), p2(250, 100) {
	nk = 10;
	h = 10;
	selected = 0;
	dotPen.setColor(blue);
	lr = 200;
	//value of y needed later on for drawspring()
	float x = (lr-20.0) / (4.0*nk);
	y =  sqrtf(h*h + x*x);
}


void SpringDrawer::mousePressEvent(QMouseEvent* me){
	selected = nearest(me->pos());
}


void SpringDrawer::mouseReleaseEvent( QMouseEvent* me){
	selected = 0;
}

void SpringDrawer::mouseMoveEvent(QMouseEvent *me){
	if (selected) *selected = me->pos();
	update();
}


void  SpringDrawer::paintEvent( QPaintEvent * ){
	QPainter paint(this);
	paint.eraseRect(rect());
	paint.setPen(dotPen);
	paint.drawEllipse(p1.x()- 3, p1.y()-2, 6, 6);
	paint.drawEllipse(p2.x()- 3, p2.y()-2, 6, 6);
	drawSpring(paint);

}

void SpringDrawer::drawSpring(QPainter& paint) {

	float dx = (float) (p1.x()- p2.x());
	float dy =  (float) (p1.y() -p2.y());

	//  find the gradient
	float m  = dy / dx ;

	//find distance between them
	float d = sqrtf(dx*dx + dy*dy);

	if( d < 2*leadLength) {
		p.drawLine(p1.x(), p1.y(), p2.x(), p2.y());
		return;
	}

	float a_x; // represent a unit vector in the direction of
	float a_y; // the spring

	float width = d - 2*leadLength;
	float s = 2*width / springCount;



}

// turns this point into a unit vector
void unitVector(QPoint& p, float* dx, float* dy) {
	float d = distance(0, 0, p.x(), p.y());
	*dx = p.x() / d;
	*dy = p.y() / d;
}

inline float distance(float x1, float y1, float x2, float y2) {

	float dx  = x1 - x2;
	float dy = y1 - y2;

	return (sqrtf(dx*dx + dy*dy));
}

// this alters the position of the point passed to it
// in such a way that the point lies on the line between
// p1 and p2, but it is a distance f from point p1.
void SpringDrawer::putPointOnLine(QPoint& p, float f){
	int tx = (int) (f*cos(theta));
	int ty = (int) (f*sin(theta));
	if(p1.x() < p2.x()) {
		p.setX(p1.x() + tx);
		p.setY(p1.y() + ty);
	}
	else {
	    p.setX(p1.x() - tx);
		p.setY(p1.y() - ty);
	}
}


//returns a pointer to 1 or 2, depending on wether p1 or p2, is
//closest to the argument
QPoint* SpringDrawer::nearest(const QPoint& p){
	int dist1 = (p.x() - p1.x())*(p.x() - p1.x()) +
		(p.y() - p1.y())*(p.y() - p1.y()) ;
	int dist2 = (p.x() - p2.x())*(p.x() - p2.x()) +
		(p.y() - p2.y())*(p.y() - p2.y()) ;
		
	if(dist1 < dist2) return &p1;
	else return &p2;	
}


int main( int argc, char **argv ) {

	QApplication app(argc, argv );
	SpringDrawer *s = new SpringDrawer();
    s->show();
    app.connect( &app, SIGNAL(lastWindowClosed()), &app, SLOT(quit()) );
    return app.exec();

 }