#include <qapplication.h>
#include <qwidget.h>
#include <qpoint.h>
#include <qpen.h>
#include <qpainter.h>
#include <math.h>
#include <iostream>

// This is a demo class for testing a spring-drawing algorithm.

class SpringDrawer : public QWidget{

public:
	SpringDrawer(QWidget* parent = 0, const char *name=0);
	void drawSpringMatt(QPainter& paint, float, float, float, float);
protected:
    virtual void mousePressEvent ( QMouseEvent * );
    virtual void mouseReleaseEvent ( QMouseEvent * );
    virtual void mouseMoveEvent( QMouseEvent *me );
    void paintEvent( QPaintEvent * );

private:
	QPoint p1;   //the two particles to draw the spring
	QPoint p2;   // between
	QPoint a;    // the bits the kinks are actually connected
	QPoint b;    // to, these are connected to the points by
	float alen;
				 // straight lines.
	int nk;		  //the number of kinks in the spring;
	int h;		 // half the width of the spring at its rest length
	float y;      //length of 1/2 of a kink at rest length
	float lr;     //rest length
	float theta;  //guess what

	QPoint* selected;
	QPoint* nearest(const QPoint&);
	void drawSpring(QPainter& paint);
	void putPointOnLine(QPoint& p, float f);
	QPen dotPen;
};

SpringDrawer::SpringDrawer(QWidget* parent, const char *name):
	QWidget(parent,name),p1(50, 100), p2(250, 100) {
	nk = 40;
	h = 10;
	selected = 0;
	dotPen.setColor(blue);
	lr = 200;
	//value of y needed later on for drawspring()
	float x = (lr-20.0) / (4.0*nk);
	y =  sqrtf(h*h + x*x);
	alen = 10;
}


void SpringDrawer::mousePressEvent(QMouseEvent* me){
	selected = nearest(me->pos());
}


void SpringDrawer::mouseReleaseEvent( QMouseEvent* me){
	selected = 0;
}

void SpringDrawer::mouseMoveEvent(QMouseEvent *me){
	if (selected) *selected = me->pos();
	update();
}


void  SpringDrawer::paintEvent( QPaintEvent * ){
	QPainter paint(this);
	paint.eraseRect(rect());
	paint.setPen(dotPen);
	paint.drawEllipse(p1.x()- 3, p1.y()-2, 6, 6);
	paint.drawEllipse(p2.x()- 3, p2.y()-2, 6, 6);
	drawSpringMatt(paint, p1.x(), p1.y(), p2.x(), p2.y());


}

//this method written by Matthew Raffety, coded by Carl Lewis
// variables: alen : length of the small lead from each point to spring
// S_H : leght of hypotenuse of triangle
void
SpringDrawer::drawSpringMatt(QPainter& paint, float pZeroX ,float pZeroY,
	float pOneX, float pOneY) {

        float deltaX = pOneX - pZeroX;
	float deltaY = pOneY - pZeroY;
	float deltaLen = sqrtf(deltaX*deltaX + deltaY*deltaY);

	if (deltaLen < 2 * alen) {
		paint.drawLine((int) pZeroX, (int) pZeroY, (int) pOneX, (int) pOneY);
		return;
	}
	float bigAX = alen * deltaX / deltaLen;
	float bigAY = alen * deltaY / deltaLen;
	float width = deltaLen - 2.0*alen;
	float littleS = 2.0* width / nk;  // nk = number of kinks
	float S_H = 40.0 ;  //should probably be a parameter

	if(littleS >= S_H)  {
		paint.drawLine((int) pZeroX, (int) pZeroY, (int) pOneX, (int) pOneY);
		return;
	}

	float vectorSX = (deltaX - 2*bigAX) / nk;
	float vectorSY = (deltaY - 2*bigAY) / nk;

	float height = sqrtf(S_H*S_H - littleS*littleS);

	float bigMX = height* deltaY / deltaLen;
	float bigMY = -height * deltaX / deltaLen;

	float DrawX = pZeroX + bigAX + ( 0.5 * vectorSX);
	float DrawY = pZeroY + bigAY + ( 0.5 * vectorSX);

	paint.moveTo((int) pZeroX, (int) pZeroY );
	paint.lineTo( (int) (pZeroX + bigAX) , (int) (pZeroY + bigAY) );

	for(int i = 0; i < nk; i++) {
		if(i & 0x1) paint.lineTo((int)(DrawX + bigMX) , (int)(DrawY + bigMY));
		else paint.lineTo( (int)(DrawX - bigMX) , (int)(DrawY - bigMY) );
		DrawX += vectorSX; DrawY += vectorSY;

	}
	paint.lineTo((int) (pOneX - bigAX), (int)(pOneY - bigAY));
	paint.lineTo((int) pOneX , (int) pOneY ) ;


}

void SpringDrawer::drawSpring(QPainter& paint) {

	float dx = (float) (p1.x()- p2.x());
	float dy =  (float) (p1.y() -p2.y());
	if(dx == 0) return ; //for now

	//  find the gradient
	float m  = dy / dx ;

	//find distance between them
	float d = sqrtf(dx*dx + dy*dy);

	// -20.0 for the striaght bits between p1, a and p2, b
	float x = (d-20.0) / (4.0*nk);
	float kinkl = (d-20.0) / nk;
	//y =  sqrtf(h*h + x*x);
	float hprime = sqrtf(y*y - x*x);

	theta = atan(m); //cout << theta << endl;
	putPointOnLine(a, 10.0);
	putPointOnLine(b, d - 10.0);

	//make a list of 10 points "close to a"
	QPoint nlist[10];
	QPoint flist[10];

	for (int i = 0; i < 10 ;i++)
		putPointOnLine(nlist[i], (10+x) +(i*kinkl));
	//make a list of 10 points "close to b"
	for(int i = 0; i < 10; i++)
		putPointOnLine(flist[i], (10-x+kinkl)+(i*kinkl));

	//displace each set of points h from the line
	float thetaPrime = atan (-1.0 / m);
	int dxp = (int) (hprime*cos(thetaPrime));
	int dyp = (int) (hprime*sin(thetaPrime));

	for(int i = 0; i < 10; i++){
		nlist[i].setX(nlist[i].x() + dxp);
		nlist[i].setY(nlist[i].y() + dyp);

		flist[i].setX(flist[i].x() - dxp);
		flist[i].setY(flist[i].y() - dyp);

	}
	//connect the points appropriately.
	for(int i = 0; i <9; i++) {
		paint.drawLine(nlist[i], flist[i]);
		paint.drawLine(flist[i], nlist[i+1]);

	}
	paint.drawLine(nlist[9], flist[9]);
	paint.drawLine(a, nlist[0]);
	paint.drawLine(b, flist[9]);
	paint.drawLine(p1, a);
	paint.drawLine(p2, b);


}

// this alters the position of the point passed to it
// in such a way that the point lies on the line between
// p1 and p2, but it is a distance f from point p1.
void SpringDrawer::putPointOnLine(QPoint& p, float f){
	int tx = (int) (f*cos(theta));
	int ty = (int) (f*sin(theta));
	if(p1.x() < p2.x()) {
		p.setX(p1.x() + tx);
		p.setY(p1.y() + ty);
	}
	else {
	    p.setX(p1.x() - tx);
		p.setY(p1.y() - ty);
	}
}


//returns a pointer to 1 or 2, depending on wether p1 or p2, is
//closest to the argument
QPoint* SpringDrawer::nearest(const QPoint& p){
	int dist1 = (p.x() - p1.x())*(p.x() - p1.x()) +
		(p.y() - p1.y())*(p.y() - p1.y()) ;
	int dist2 = (p.x() - p2.x())*(p.x() - p2.x()) +
		(p.y() - p2.y())*(p.y() - p2.y()) ;

	if(dist1 < dist2) return &p1;
	else return &p2;
}


int main( int argc, char **argv ) {

	QApplication app(argc, argv );
	SpringDrawer *s = new SpringDrawer();
    s->show();
    app.connect( &app, SIGNAL(lastWindowClosed()), &app, SLOT(quit()) );
    return app.exec();

 }


