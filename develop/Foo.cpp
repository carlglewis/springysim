class Foo {

	int f;
	friend Foo operator+(Foo& left, Foo& right);
};


Foo operator+(Foo& left, Foo& right) {
	Foo result;
	result.f = left.f + right.f;
	return result;
}


