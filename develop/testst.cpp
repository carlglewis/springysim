#include "../src/SpringType.h"
#include <iostream>
#include <qfile.h>

int main() {
    QColor col(230, 50, 30);
	SpringType a("Stiff", col, 1.5, 0.2, 200);
	col.setRgb(200, 20, 10);
	SpringType b("Slack",col, 0.3, 0.2, 200);
	
	col.setRgb(100, 10, 10);
	SpringType c("High Damping",col, 0.5, 0.5, 200);
	col.setRgb(10, 255, 10);
	SpringType g("Low Damping",col, 0.5, 0.05, 200);
	
	col.setRgb(10, 10, 255);
	SpringType h("Long",col, 0.5, 0.2, 300);
	col.setRgb(10, 150, 180);
	SpringType k("Short", col, 0.5, 0.2, 100);
	
	QFile f( "/home/carl/.springTypes" );

    if(!f.open( IO_WriteOnly )) {
    	cout << "Could not open file to write!!" << endl;
    	exit(1);
    }
    QDataStream s( &f );
	s << a << b << c << g << h << k;	
	f.close();
	
	if(!f.open(IO_ReadOnly)) {
		cout << "Could not open file to read!!" << endl;
    	exit(1);
	
	}
	
	SpringType d, e;
	s.setDevice(&f);
	s >> d;
	s >> e;
	
	cout << d << e;
}