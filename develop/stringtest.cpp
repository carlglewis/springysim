#include <iostream>
#include <qfile.h>
#include <qstring.h>
#include <qdatastream.h>


int main(){

   QFile f( "./springtestfile" );

    if(!f.open( IO_WriteOnly )) {
    	cout << "Could not open file to write!!" << endl;
    	exit(1);
    }
    QDataStream qds( &f );
    QString s = "This is a test";

    qds << s;
    f.close();

    if(!f.open(IO_ReadOnly)) {
		cout << "Could not open file to read!!" << endl;
    	exit(1);
	
	}

	QString s2;
	qds.setDevice(&f);
	
	qds >> s2;
	cout << "s2: "<< s2 << endl;
	f.close();
	
}