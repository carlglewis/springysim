#include "Force.h"



Const_Field::Const_Field(ParticleSystem* p, float x, float y, float z){
    direction = new float[3];
  	direction[0] = x;
  	direction[1] = y;
  	direction[2] = z;
    active = true;
    ps = p;
}


Const_Field::Const_Field(ParticleSystem* p, QDataStream& qds){
	direction = new float[3];
	for(int i=0;i<3;i++) qds >> direction[i];
	int b; qds >> b;
	active = static_cast<bool>(b);
	ps = p;	
}


Const_Field::Const_Field(const Const_Field & f) {
	direction = new float[3];
	this->operator=(f);
}


Const_Field& Const_Field::operator=(const Const_Field & f){
	if (this != &f) {
		direction[0] = f.direction[0];
  		direction[1] = f.direction[1];
  		direction[2] = f.direction[2];
  		active = f.active;	
	}
    return *this;
}

inline bool Const_Field::isActive() const { return active; }

inline void Const_Field::activation(bool b) {
	active = b;	
}

void Const_Field::applyForce() const {
	if (!active) return ;	
	Particle* p;
	for (unsigned int i = 0; i < ps->size(); i++) {
		p = (*ps)[i];
		p->x[Particle::FORCE] += direction[0];		
	    p->x[Particle::FORCE+1] += direction[1];
	    p->x[Particle::FORCE+2] += direction[2];
	}
}


QDataStream& Const_Field::operator<<(QDataStream& qds){
	qds << ParticleSystem::CONST_FIELD;
	qds << direction[0] << direction[1] << direction[2];
	qds << (int) active;
	return qds;	
}


//-----------------------------------------------------------
//  Spring methods
//-----------------------------------------------------------

Spring::Spring(Particle* first, Particle* second,float springConstant, float dampingFactor,
	float restLength): p1(first), p2(second),
	ks(springConstant), kd(dampingFactor), r(restLength),
	col(0, 0, 0){}

		
Spring::Spring(ParticleSystem* ps,QDataStream& qds){
	int i; qds >> i;
	p1 = ps->getParticle(i);
	qds >> i;
	p2 = ps->getParticle(i);
	qds >> ks >> kd >> r >> col;
}	
	

Spring::Spring(const Spring& s) {
	this->operator=(s);
}


Spring& Spring::operator=(const Spring& s){
	if (this != &s) {
		p1 = s.p1; p2 = s.p2;
		ks = s.ks; kd = s.kd; r = s.r;
	}
	return *this;
}
	
				
inline void Spring::setSpringConstant(float val) {
	ks = val;	
}

inline float Spring::getSpringConstant() const {
	return ks;
}

inline void Spring::setDampingFactor(float val) {
	kd = val;
}

inline float Spring::getDampingFactor() const {
	return kd;
}

inline void Spring::setrestLength(float val) {
	r = val;
}

inline float Spring::getrestLength() const {
	return r;
}


const QColor& Spring::getColor() const {
	return col;
}


void Spring::setColor(const QColor & c){
	col = c;	
}


void Spring::applyForce() const {
	
    float l[3];
    float ldot[3];
    float norml;
    float dotProd, scalar;
    //cout << p1 << p2;
    l[0] = p1->x[Particle::POSN] - p2->x[Particle::POSN];
    l[1] = p1->x[Particle::POSN+1] - p2->x[Particle::POSN+1];
    l[2] = p1->x[Particle::POSN+2] - p2->x[Particle::POSN+2];
    ldot[0] = p1->x[Particle::VEL] - p2->x[Particle::VEL];
    ldot[1] = p1->x[Particle::VEL+1] - p2->x[Particle::VEL+1];
    ldot[2] = p1->x[Particle::VEL+2] - p2->x[Particle::VEL+2];
    //cout << "ldot[i]: " << ldot[0] << " " << ldot[1] << endl;
    norml = sqrtf( l[0]*l[0] + l[1]*l[1] + l[2]*l[2]);
    dotProd = ldot[0]*(l[0]) + ldot[1]*(l[1]) + ldot[2]*(l[2]);

    scalar =  - (ks*(norml - r) + kd*(dotProd / norml)) / norml;
    //cout << "norml : " << norml << endl;
    //cout << "dotProd : " << dotProd << endl;
    //cout << "scalar: " << scalar << endl;
    //cout << "l[i] : " << l[0] << " " <<l[1] << endl << endl;


    p1->x[Particle::FORCE] += scalar * l[0];
    p2->x[Particle::FORCE] -= scalar * l[0];
    p1->x[Particle::FORCE+1] += scalar * l[1];
    p2->x[Particle::FORCE+1] -= scalar * l[1];
    p1->x[Particle::FORCE+2] += scalar * l[2];
    p2->x[Particle::FORCE+2] -= scalar * l[2];
    //cout << p1 << p2;
}
	

bool Spring::usesParticle(const Particle* p) const {
	if (p == p1 || p == p2) return true;
	else return false;	

}


QDataStream& Spring::operator<<(QDataStream& qds){
	qds << ParticleSystem::SPRING;
	qds << p1->id() << p2->id();
	qds << ks << kd << r << col;
	return qds;
}


//-----------------------------------------------------------
//  Global_Drag methods
//-----------------------------------------------------------

Global_Drag::Global_Drag(ParticleSystem* p, float coeffOfDrag) :
	 coeff(coeffOfDrag), ps(p) {}
	
	
Global_Drag::Global_Drag(ParticleSystem* p,QDataStream& qds){
	qds >> coeff;
	ps = p;
}
	

void Global_Drag::applyForce() const {
    Particle* p;
    for (unsigned int i = 0; i < ps->size(); i++) {
    	p = (*ps)[i];
    	p->x[Particle::FORCE] -= coeff*(p->x[Particle::VEL]);
    	p->x[Particle::FORCE+1] -= coeff*(p->x[Particle::VEL+1]);
    	p->x[Particle::FORCE+2] -= coeff*(p->x[Particle::VEL+2]);
    }
}


QDataStream& Global_Drag::operator<<(QDataStream& qds){
	qds << ParticleSystem::GLOBAL_DRAG;
	qds << coeff;
	return qds;	
}


//-----------------------------------------------------------
//  Drag methods
//-----------------------------------------------------------

Drag::Drag(Particle* target, float coeffOfDrag) :
	p(target), coeff(coeffOfDrag) {}
	
	
Drag::Drag(ParticleSystem* ps, QDataStream& qds){
	int i;  qds >> i;
	p = ps->getParticle(i);
	qds >> coeff;	
}	
	
	
inline float Drag::getCoeff() const {
	return coeff;
}	


inline void Drag::setCoeff(float val) {
	coeff = val;
}


void Drag::applyForce() const {
	p->x[Particle::FORCE] -= coeff * (p->x[Particle::VEL]);
	p->x[Particle::FORCE+1] -= coeff * (p->x[Particle::VEL+1]);
	p->x[Particle::FORCE+2] -= coeff * (p->x[Particle::VEL+2]);
}


bool Drag::usesParticle(const Particle* pc) const {
	if (pc == p) return true;
	else return false; 	
}


QDataStream& Drag::operator<<(QDataStream& qds){
	qds <<  ParticleSystem::DRAG;
	qds << p->id();
	qds << coeff;
	return qds;
}

//-----------------------------------------------------------
//  Constant methods
//-----------------------------------------------------------


Constant::Constant(Particle* target, float xval,
	float yval, float zval) :
	p(target) {
	f[0] = xval;
	f[1] = yval;
	f[2] = zval;	
}
	
Constant::Constant(ParticleSystem* ps, QDataStream& qds){
	int i; qds >> i;
	p = ps->getParticle(i);
	for(i=0;i<3;i++) qds>>f[i];	
}


inline float Constant::getVal(int whichval) const {
	if(whichval < 0 || whichval > 2) return 0.0;
	return f[whichval];
}


inline void Constant::setVal(float newval, int whichval) {
	if(whichval < 0 || whichval > 2) return;
	f[whichval] = newval;
}


void Constant::applyForce() const {
	p->x[Particle::FORCE] += f[0];
    p->x[Particle::FORCE+1] += f[1];
    p->x[Particle::FORCE+2] += f[2];

}


bool Constant::usesParticle(const Particle* pc) const {
	if (pc == p) return true;
	else return false; 	
}


QDataStream& Constant::operator<<(QDataStream& qds){
	qds <<  ParticleSystem::CONSTANT;
	qds << p->id();
	for(int i =0;i < 3;i++) qds << f[i];
	return qds;

}


QDataStream& operator<<(QDataStream& qds, Force& f){
	f.operator<<(qds);
	return qds;
}



