#ifndef cl_plane
#define cl_plane
#include "Particle.h"
#include <iostream>
#include <stdlib.h>
#include <cmath>

#define PR(x) cout << #x " = " << x << "\n";

class D2_Collider {

public:
	D2_Collider(ParticleSystem* parent);
	virtual ~D2_Collider(){}
	D2_Collider(const D2_Collider& d);
	virtual D2_Collider& operator=(const D2_Collider&);
    virtual void solveCollisions() = 0;

protected:
	ParticleSystem* ps;
};

class D2_Plane : public D2_Collider{

public:

	//note that normal muat be a unit vector!
	D2_Plane(ParticleSystem* parent, float* position,
		float* normal, float coeffOfRest);
	virtual ~D2_Plane();
	D2_Plane(const D2_Plane& p);
	virtual D2_Plane& operator=(const D2_Plane&);
	virtual void solveCollisions();
	
	void coeff(float val);
	float coeff() const ;
	
private:
	float r;
	float* posn;
	float* norm;
};


#endif



