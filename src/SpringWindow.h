/****************************************************************************
** $Id: SpringWindow.h,v 1.3 2004/06/06 08:46:37 carl Exp $
**
** Copyright (C) 1992-2000 Troll Tech AS.  All rights reserved.
**
** This file is part of an example program for Qt.  This example
** program may be used, distributed and modified without limitation.
**
*****************************************************************************/

#ifndef CL_SPRWIN
#define CL_SPRWIN_H

#include <qmainwindow.h>
#include "ParticleField.h"
#include "ConstraintChooser.h"
#include "SimToolBar.h"
#include "SpringType.h"

class QMultiLineEdit;
class QToolBar;
class QPopupMenu;

class SpringWindow: public QMainWindow
{
    Q_OBJECT
public:
	SpringWindow();
	~SpringWindow();
	
public slots:
	void updateSimMenu();
	void updateEditMenu();
	void updateSolverMenu();
    
protected:
	//void closeEvent( QCloseEvent* );

private slots:
	void save();
	void saveAs();
	void openFile();
	void openMenuAction();
	void newDoc();
	void about();
	void aboutQt();
	void message(char*);
	void showDialog();
	void gridSize();
	
private:	
	SimToolBar *simTools;
	SpringToolBar *springTools;
	ParticleField *pf;
	ConstraintChooser* conDialog;
	QPopupMenu* d, *edit, *solvers, *view;
	QString currentFile;
	//void getFileName();
	bool saveFile(QString& fileName);
	QString& truncateFilename(QString& s);
};

#endif
