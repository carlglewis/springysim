#include "SpringType.h"

SpringType::SpringType(const QString& name, const QColor& color,
	float iks, float ikd, float restlength) :
		col(color), n(name), ks(iks), kd(ikd), len(restlength){}
		
		
//no color specified means default color is black.		
SpringType::SpringType(const QString& name, float iks,float ikd,
	 float restlength) :
		col(0,0,0), n(name) ,ks(iks), kd(ikd), len(restlength){}
		
		
void SpringType::setColor(QColor& newColor) {
	col = newColor;
}


void SpringType::setColor(int red, int green, int blue) {
	col.setRgb(red, green, blue);	
}


const QColor& SpringType::getColor() const{
	return col;
}


void SpringType::setName(const QString & str) {
	n = str;
}

const QString& SpringType::getName() const {
	return n;
}

void SpringType::setCoeffs(float* array) {
	if(array[0] >= 0) ks = array[0];
	if(array[1] >= 0) ks = array[1];
	if(array[2] >= 0) ks = array[2];
}

const void SpringType::getCoeffs(float* springk, float* damping,
		float* restlen) const {
	*springk = ks;
	*damping = kd;
	*restlen = len;				
}


// global functions
QDataStream& operator<<(QDataStream& ds, const SpringType& st) {
	ds << st.col << st.n << st.ks << st.kd << st.len;
	return ds;
}


QDataStream& operator>>(QDataStream& ds,SpringType& st) {
	ds >> st.col >> st.n >> st.ks >> st.kd >> st.len;
	return ds;
}


std::ostream& operator<<(std::ostream& os, const SpringType& st) {
    os << st.n.latin1() << std::endl << st.ks << " " << st.kd << " "<<st.len << " ";
    int r, g, b;
    st.col.rgb(&r, &g, &b);
    os << r <<" " << g << " " << b  << std::endl;
    return os;
}
