#include <qdialog.h>
#include <qlistbox.h>
#include <qlineedit.h>
#include <qpushbutton.h>
#include <iostream>
#include <qlayout.h>

#ifndef CON_CHOOSER
#define CON_CHOOSER

enum ConstraintType {HORIZ, VERT, FIXED, DISTANCE, CIRCLE,
	DEF_DISTANCE};

class ConstraintChooser : public QDialog {
    Q_OBJECT
public:

    ConstraintChooser( QWidget * parent = 0, const char * name = 0);


public slots:
	void numParticlesSelected(unsigned int n);
	void getInput();
	
signals:
	void makeConstraint(ConstraintType c, float data);
	
private:
	QListBox *list;
	QLineEdit *input;
	QPushButton *make;
	//QPushButton* cancel;
	void addMost();
	bool getNumInput();
	void makeDistance();
	float f;

};


#endif


