#ifndef cl_m_spring
#define cl_m_spring
#include <iostream>
#include "Force.h"
#include "Particle.h"

// a fairly dodgy piece of inheritance
// don't call the Spring::getParticle() routines
// on a Mouse_Spring.
// Mouse_Spring alocates memory for one of its 'particles'
// which represets the pointer when solving the spring equation
// hence the non-empty destructor.

class Mouse_Spring: public Force {
	ParticleSystem* ps;
	bool active;
	static const float r = 0;
	float ks; float kd;
	float x, y;
	Particle* p1;
	
public:	
	Mouse_Spring(ParticleSystem * psys);
	~Mouse_Spring();
	bool isActive() const;
	virtual void applyForce() const;
	void pickParticle(int mousex, int mousey);
	void update_posn(int mousex, int mousey);
	void releaseParticle();	
	inline Particle* getTarget() const { return p1; }
};

#endif


