


#ifndef CL_SPRINGTYPE
#define CL_SPRINGTYPE

#include <qtoolbar.h>
#include <qcombobox.h>
#include <qspinbox.h>
#include <qlayout.h>
#include <qpushbutton.h>
#include <qlabel.h>
#include <qstring.h>
#include <qcolor.h>
#include <qdatastream.h>
#include <iostream>
#include <qfile.h>
#include <list>
#include "CustomSpinBox.h"
#include "Environ.h"

#define SPRING_TYPE_FILE "springtypes"

class SpringType {

public:
	SpringType(const QString& name, const QColor& color,
		float iks=0.5, float ikd=0.5, float restlength=100.0 );
 	SpringType(const QString& name = "", float
		iks=0.5, float ikd=0.5,	float restlength=100.0);

	void setColor(QColor &);
	// 0 < all args < 255
	void setColor(int red, int green, int blue);
	const QColor & getColor() const ;
	
	void setName(const QString &);
	const QString& getName() const ;
	
	// pass a length 3 array.  If any of the
	// entries are negative, they are left unchanged.
	// order is spring constant, damping, length
	void setCoeffs(float* array);
	const void getCoeffs(float* springk, float* damping,
		float* restlen) const;
		
	const float getKs(){return ks;}
	const float getKd(){return kd;}
	const float getLen(){return len;}
	
	void setKs(float newval){ks = newval;}
	void setKd(float newval){kd = newval;}	
	void setLen(float newval){len = newval;}	
	
	// global functions
	friend QDataStream& operator<<(QDataStream& ds,
		const SpringType& st);
	friend QDataStream& operator>>(QDataStream& ds,
		SpringType& st); 	
	friend std::ostream& operator<< (std::ostream& os, const SpringType& st);
					
private:
	QColor col;
	QString n;
	float ks;
	float kd;
	float len;
	
};





class ColorChit : public QWidget {
 	Q_OBJECT
public:
    ColorChit(QWidget * parent=0, const char * name=0);
    const QColor & color() const;
    void color(const QColor& color);
    virtual void mouseDoubleClickEvent(QMouseEvent* me);

signals:
	void colorChanged(QColor & col);

private:
    QColor c;


};


class SpringToolBar : public QToolBar {

	Q_OBJECT
public:
      SpringToolBar(QMainWindow * parent = 0, const char * name = 0);
      ~SpringToolBar();

      //called right after connecting slots;
      void init();

public slots:
	void typeChanged(int index);
	void newSpringType();
	void ksChanged(float val);
	void kdChanged(float val);
	void lenChanged(float val);
	void colorChanged(QColor& col);
	
signals:
	void typeAltered(SpringType* st);

private:
    QComboBox* cb;
    ZeroTo500* rl;
    ZeroToTen* ks;
    ZeroToOne* kd;
    QPushButton* newtype;
    QLabel *a, *b, *c;
    ColorChit * chit;
    std::list<SpringType*>* types;
    SpringType* current; //currently selected type
    int newTypeCount;    // for making new names

    typedef std::list<SpringType*>::iterator Iter;

	void loadDefaults();
    void nameChanged(int pos, const QString& s);
};


/*

class SpringDialog : public QDialog {
	Q_OBJECT
public:
	 SpringDialog(QWidget * parent = 0, const char * name = 0);
	 virtual ~SpringDialog();

};
  */

#endif


