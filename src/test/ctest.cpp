#include "Particle.h"
#include "Constraint.h"
#include "Euler.h"
#include "matrix/Matrix.h"
#include "matrix/matrix_io.h"

////////////////////////////////
// test harness for constraint system.

int main() {
    Matrix::Vector<float> a(4);
    a.init(1.0f);
    cout<< a << endl;
	
	ParticleSystem ps;
	ConstraintSystem *cs = ps.enableConstraints(true);
	
	Euler e(&ps);
    Particle *p1, *p2, *p3;
    p1 = ps.addParticle(0.0, 0.0, 0.0);
    p2 = ps.addParticle(2.0, 0.0, 0.0);
    p3 = ps.addParticle(4.0, 0.0, 0.0);
    cs->addDistConstraint(*p1, *p2, 2.0f);
    cs->addDistConstraint(*p2, *p3, 2.0f);
    Force* f = new Constant(p1, 1.0, 0.0, 0.0);
    ps.addForce(f);
    cout << "p1: " << p1 << endl;
    cout << "p2: " << p2 << endl;
    try{
    	e.update(0.1);
    }
    catch(Matrix::Error &e) {
    	cout << "Matrix exception caught" << endl;
    	cout << e.message() << endl;
    }


	cout << "p1: " << p1 << endl;
    cout << "p2: " << p2 << endl;
    cout << "p3: " << p3 << endl;
}

