#include "ConstraintChooser.h"

ConstraintChooser::ConstraintChooser( QWidget * parent,
	const char * name): QDialog(parent, name){
	setCaption("Add Constraint");
	QGridLayout * g = new QGridLayout( this, 2, 2, 6 );
	
	list = new QListBox(this, "list");
	list->setMinimumHeight(80);
	input = new QLineEdit(this, "input");
	make = new QPushButton("Make Constraint", this, "make");
	
	g->addWidget( list, 1, 1 );
	g->addWidget( make, 2, 1 );
	g->addWidget(input, 1, 2) ;
    list->setFocusPolicy( QWidget::StrongFocus );

    connect(make, SIGNAL(clicked()), this, SLOT(getInput()));
    f = -1.0;	
}


void ConstraintChooser::numParticlesSelected(unsigned int n) {
	list->clear();
	switch(n) {
		case 0 :input->setEnabled(false);
				return;
		case 2 : list->insertItem("Distance");
				input->setEnabled(true);
				addMost();
				break ;
		case 1 : list->insertItem("Circle");
				 input->setEnabled(true);
				 addMost();
				 break;				
		default: addMost();
				 input->setEnabled(false);
	}
	
}
	
void ConstraintChooser::addMost() {
	list->insertItem("Fixed");
	list->insertItem("Horizontal Plane");
	list->insertItem("Vertical Plane");
}

void ConstraintChooser::getInput() {
	QString s = list->currentText();
	if(s == "Fixed" )
		emit makeConstraint(FIXED, 0);
	else if( s == "Horizontal Plane")
		emit makeConstraint(HORIZ, 0);
	else if(s == "Vertical Plane")
		emit makeConstraint(VERT, 0);
	else if(s == "Circle"){
		if(getNumInput())
			emit makeConstraint(CIRCLE, f);
	}
	else if(s == "Distance") makeDistance();
}


bool ConstraintChooser::getNumInput() {
	QString s = input->text();
	bool b = false;
	f = s.toFloat(&b);
	if(b && f < 0.0) b = false; // no negatives
	return b ;
}


void ConstraintChooser::makeDistance() {
	QString s = input->text();
	if(s.isEmpty())
		emit makeConstraint(DEF_DISTANCE, 0);
	else if(getNumInput())
		emit makeConstraint(DISTANCE, f);
}
