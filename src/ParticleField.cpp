
#include "ParticleField.h"
#include <qpainter.h>
#include <math.h>
#include <qapplication.h>
#include <qcolor.h>
#include <matrix/Matrix.h>


ParticleField::ParticleField( QWidget *parent, const char *name )
        : QWidget( parent, name )
{
    setPalette( QPalette( QColor( 250, 250, 200) ) );
    ps = new ParticleSystem();
    cs = ps->enableConstraints(true);
   	
   	gravity = new Const_Field(ps, 0.0, 35.0, 0.0);
   	ps->addForce(gravity);
   	
    ms = new Mouse_Spring(ps); //don't delete!!
    ps->addForce(ms);

    drag = new Global_Drag(ps);
    ps->addForce(drag);

    e = new Euler(ps);
    st = EULER;

    clicked = 0;
    deltaT = 0.06;
    interval = 30;
    leftButtonPressed = false;
    sprType = 0;

	running = false;
    timer = new QTimer(this);
    connect( timer, SIGNAL(timeout()),
             this, SLOT(update()) );

    setFocusPolicy(QWidget::StrongFocus);

    bg = new QColor(220, 180, 240);
    bgBrush = new QBrush(*bg);

    // set up modes
    mode = CONSTRAINTS; mousemode = SELECT;

    // Keeps track of selected particles
    selected = new std::set<Particle*>();

    // no tracing initially
    tracer = 0;
    tracing = false; drawTrace = false;
    tracePen.setRgb(40, 20, 20);

    //no grid
    snapToGrid = false;  displayGrid = false;
    gridSize = 50;

    buildBorders();
    setFixedSize(600, 600);
    setBackgroundMode(NoBackground);
    drawComplexSpring = true;
}

ParticleField::~ParticleField()  {
	delete ps;
	delete e;
	delete bg;
	//delete bgBrush;
}

void ParticleField::mousePressEvent ( QMouseEvent * me) {

     thisx = me->x();  thisy = me->y();

    // nothing happens on right mouse press
    if (me->button() == RightButton) return;

    leftButtonPressed = true;
	if(me->state() & ControlButton) {
		//grap the particle
		ms->pickParticle(me->x(), me->y());
	}
	else {
		if(mousemode == MAKESPRING) {
			// going to build a spring
			Particle* p = selectParticle(me);
			if (p == 0) return;
			clicked = p;
			
		}
		else {
			//record corner of the selection
			// rectangle
			startx = me->x();
			starty = me->y();
		}
	}
	
}


void ParticleField::mouseReleaseEvent ( QMouseEvent * me) {

	// RIGHT BUTTON
    if (me->button() == RightButton)  {
    	// create a new Particle.
    	addParticle(me->x(), me->y(),
    		(bool) (me->state() & ShiftButton)); 	
    }

    // LEFT BUTTON
    else if (me->button() == LeftButton) {
    	if(mousemode == MAKESPRING) {
			makeSpring(me);
		}
		else {
			// select all the particles that are
			// in the box
			if(!(me->state() & ShiftButton)) selected->clear();
			//because we don't really
			if(!ms->isActive())	
				boxSelect(me);		
		}
		leftButtonPressed = false;
		clicked = 0;             // kill partial spring
		ms->releaseParticle();  //kill the mousespring
    }

    // repaint window (BOTH BUTTONS)
    QPaintEvent* pev = new QPaintEvent(rect());
    paintEvent(pev);
    delete pev;	
}


void ParticleField::paintEvent( QPaintEvent * ) {

    QPainter painter( &pix );
	//draw the background.
	if(drawTrace) {
		QPoint p(0,0);
		bitBlt(&pix, p, &tracemap, rect(), CopyROP);
	}
	else
		painter.fillRect(rect(),*bgBrush);
		
    if(displayGrid) drawGrid(painter);
    drawSprings(painter);
    drawConstraints(painter);
	drawParticles(painter);
	drawMouseArt(painter);
    painter.end();
    painter.begin( this );
    painter.drawPixmap( rect().topLeft(), pix );
}


void ParticleField::mouseMoveEvent( QMouseEvent *me )
// if the sim is running, we just have to note
// the current mouse pos.

// if the sim is stopped, we still have to do these things
// but have to generate a paintevent as well.
{
	thisx = me->x(); thisy = me->y();
	
    if ( leftButtonPressed ) {
    	// also tell the mousespring about it
    	if (ms->isActive()) ms->update_posn(me->x(), me->y());

    	// generate paintEvent
		if(!running) {
			QPaintEvent * pe = new QPaintEvent(this->rect());
			paintEvent(pe);
			delete pe;
    	}
   }
}


void ParticleField::keyPressEvent ( QKeyEvent * ke ) {

	int val = ke->key();
	switch(val){
		case Key_G : runSim(!running);// start if stopped
					 break;   		  // and vice versa	
		case Key_S : if (running) runSim(false); //stop
					 else update(); //just do one step	
					 break;
		case Key_Delete : deleteSelectedParticles();
						  break;
	    default: ke->ignore();	    	
	}
}

/*
QSizePolicy ParticleField::sizePolicy() const
{
    return QSizePolicy( QSizePolicy::Expanding, QSizePolicy::Expanding );
} */



Particle* ParticleField::selectParticle( QMouseEvent * me) {
	Particle* p = 0;
	Particle* r = 0;
	for (unsigned int i = 0; i < ps->size(); i++) {
    	p = (*ps)[i];
    	if ( pow((p->x[0] - (float)me->x()), 2) +
    		pow((p->x[1] - (float)me->y()), 2) < 100.0 ) {
    	     r = p; break;			
    	}    				
    }
    return r;
}


void ParticleField::drawParticles(QPainter & painter) {
     Particle* p;

	//std::cout << selected->size() << std::endl;
	painter.setPen(NoPen);
    for (unsigned int i = 0; i < ps->size(); i++) {
    	p = (*ps)[i];
    	
    	// draw a highlight on selected particles
    	if(selected->find(p) != selected->end()) {
    		painter.setBrush(white);
    		painter.drawPie((int)p->x[0] - 7, (int)p->x[1] - 7,
    			14, 14,0, 5760);
    	}
    	
    	if (p->isFixed()) painter.setBrush(red);
    	else painter.setBrush(blue);
    	painter.drawPie((int)p->x[0] - 5, (int)p->x[1] - 5,
    		10, 10,0, 5760);
    }

}


void ParticleField::drawSprings(QPainter& painter) {
	painter.setPen(Qt::SolidLine) ;
	const Force* f = 0;
	const Spring* s = 0;
	const Particle* p1;const Particle* p2;
	int max = ps->numForces();
	for (int i = 0; i < max ;i++ ) {
		f = ps->getForce(i);
		s = dynamic_cast<const Spring*>(f);
		if(!s) continue;
		p1 = s->getParticle1();
		p2 = s->getParticle2();
		painter.setPen(s->getColor());
		if(drawComplexSpring)
			drawSpringMatt(painter, p1->x[0], p1->x[1], p2->x[0],
				p2->x[1]);
		else
			painter.drawLine((int) p1->x[0],(int) p1->x[1], 
				(int) p2->x[0], (int) p2->x[1]);
		s = 0;
    }
}

//this method written by Matthew Raffety, coded by Carl Lewis
// variables: alen : length of the small lead from each point to spring
// S_H : leght of hypotenuse of triangle
void
ParticleField::drawSpringMatt(QPainter& paint, float pZeroX ,float pZeroY,
	float pOneX, float pOneY) {
	float alen = 20; float nk = 40;
        float deltaX = pOneX - pZeroX;
	float deltaY = pOneY - pZeroY;
	float deltaLen = sqrtf(deltaX*deltaX + deltaY*deltaY);

	if (deltaLen < 2 * alen) {
		paint.drawLine((int) pZeroX, (int) pZeroY, (int) pOneX, (int) pOneY);
		return;
	}
	float bigAX = alen * deltaX / deltaLen;
	float bigAY = alen * deltaY / deltaLen;
	float width = deltaLen - 2.0*alen;
	float littleS = 2.0* width / nk;  // nk = number of kinks
	float S_H = 30.0 ;  //should probably be a parameter

	if(littleS >= S_H)  {
		paint.drawLine((int) pZeroX, (int) pZeroY, (int) pOneX, (int) pOneY);
		return;
	}

	float vectorSX = (deltaX - 2*bigAX) / nk;
	float vectorSY = (deltaY - 2*bigAY) / nk;

	float height = sqrtf(S_H*S_H - littleS*littleS);

	float bigMX = height* deltaY / deltaLen;
	float bigMY = -height * deltaX / deltaLen;

	float DrawX = pZeroX + bigAX + ( 0.5 * vectorSX);
	float DrawY = pZeroY + bigAY + ( 0.5 * vectorSX);

	paint.moveTo((int) pZeroX, (int) pZeroY );
	paint.lineTo( (int) (pZeroX + bigAX) , (int) (pZeroY + bigAY) );

	for(int i = 0; i < nk; i++) {
		if(i & 0x1) paint.lineTo((int)(DrawX + bigMX) , (int)(DrawY + bigMY));
		else paint.lineTo( (int)(DrawX - bigMX) , (int)(DrawY - bigMY) );
		DrawX += vectorSX; DrawY += vectorSY;

	}
	paint.lineTo((int) (pOneX - bigAX), (int)(pOneY - bigAY));
	paint.lineTo((int) pOneX , (int) pOneY ) ;


}


void ParticleField::drawBox(QPainter& painter) {
	if(mousemode != SELECT || leftButtonPressed == false) return ;
	painter.setBrush(NoBrush);
	painter.setPen(Qt::SolidLine);
        	painter.drawRect(startx, starty,
        		(thisx- startx), (thisy - starty) );
}


void ParticleField::drawMouseArt(QPainter & painter) {
// This method decides if either the selection box,
// MouseSpring, or a "partial spring" needs to be
// drawn and then does the drawing.
    if (!leftButtonPressed) return;

	if (ms->isActive()) {
    	painter.setPen(red);
    	Particle* const p = ms->getTarget();
    	painter.drawLine((int)p->x[Particle::POSN],
    		(int)p->x[Particle::POSN+1], thisx, thisy);
    	return;
	}
	
	if (mousemode == MAKESPRING) {
		//connect the pointer with particle
		if (clicked != 0) {
			if(sprType) painter.setPen(sprType->getColor());
			else painter.setPen(Qt::SolidLine);
			painter.drawLine((int)clicked->x[0],(int)clicked->x[1],
			thisx, thisy);
 		}
 	}
	else {
		// draw the selection box.
		drawBox(painter);
	}

}


void ParticleField::drawConstraints(QPainter& p) {
	if(!cs) return;
	unsigned int max = cs->size();
	const Constraint* c = 0;
	const Distance * d = 0;
	const Circle* circ = 0;
	int x1, y1, x2, y2;
	QPen pen(black, 2);
	p.setPen(pen);	
	p.setBrush(NoBrush);	
	for (unsigned int i = 0; i < max ;i++) {
		c = cs->getConstraint(i);
		if (c) d = dynamic_cast<const Distance*>(c);
		if(d) {
			const Particle& p1 = d->getParticle1();
			const Particle& p2 = d->getParticle2();
			x1 = (int) p1.x[Particle::POSN];
			y1 = (int) p1.x[Particle::POSN+1];
			x2 = (int) p2.x[Particle::POSN];
			y2 = (int) p2.x[Particle::POSN+1];
			p.setPen(pen);
			p.drawLine(x1, y1,x2, y2);	
		}
	else {
			if(c) circ = dynamic_cast<const Circle*>(c);
			if(circ) {
			const Circle::Data& cd = circ->getData();
			float r  = sqrt(cd.r);
			//std::cout << cd.x << " "<< cd.y <<" " << cd.z<< 
				//" " << cd.r << std::endl;
		
			p.drawEllipse((int) (cd.x - r),(int) (cd.y - r),
				(int) (2*r), (int) (2*r));
			}	
		}

		d = 0; c= 0;circ = 0;
	}
}


void ParticleField::drawGrid(QPainter& p){
	p.setPen(white);
	//horizontal lines
	int h = height(); int w = width();
	for(int i = 0; i < h; i+=gridSize)
		p.drawLine(0, i, w, i);
	//vertical lines
	for(int j = 0;j < w;j+= gridSize)
		p.drawLine(j,0,j, h);	
}

void ParticleField::update() {

	try {
		e->update(deltaT);  // do the physics
	}
	catch(Matrix::Error& e) {
		std::cerr << "Simulation Aborted"<< std::endl;
		runSim(false);
		emit requestReload();
	}
	
	//deal with tracer
	if(tracing) updateTrace();
	
	//draw the screen
	QPaintEvent* pev = new QPaintEvent(rect());
	paintEvent(pev);
	delete pev;	
}


void ParticleField::setDeltaT(float val) {
	deltaT = val;
}


void ParticleField::init() {
    QSize s = size();
    pix.resize(s);
    tracemap.resize(s);

    // put a background on the tracemap
    QPainter p(&tracemap);
    p.fillRect(rect(),*bgBrush);
}


void ParticleField::clearAll() {
	if(ps) ps->clear();
	if(cs) cs->clear();
	if(tracing) toggleTrace();
	ms->releaseParticle();
}


void ParticleField::clearSprings() {
	ps->clearSprings();
}


void ParticleField::setViscousDrag(float val) {
	drag->setCoeff(val);
	emit action("viscous drag changed");
}


void ParticleField::setGravity(int val) {
	gravity->setY((float)val);
	emit action("gravity changed");
}


void ParticleField::setWallRestitution(float val){
	float b;
	if(val < 0) b = val;
	else b = -val;
   for (int i = 0;i < 4; i++) {
    	borders[i]->coeff(b);
   }

}

void ParticleField::setUpdateTime(int val){
	interval = val;
	if(running) timer->changeInterval(interval);
}

void ParticleField::getToolBarSettings(SimData* sd){

	sd->dt = deltaT;
	sd->du = interval;
	sd->gravity = (int)gravity->getY();
	sd->wallrest = borders[0]->coeff();
	sd->drag = drag->getCoeff();
	
}


void ParticleField::setSpringType(SpringType* ns) {
	sprType = ns;
}


void ParticleField::buildBorders() {

	//ground
	float* posn = new float[2];
    posn[0] = 300.0; posn[1] = 600.0;
    float* normal  = new float[2];
    normal[0] = 0.0; normal[1] = -1.0;
    borders[0] = new D2_Plane(ps, posn, normal, -0.7);

    //ceiling
    posn = new float[2];
    posn[0] = 300.0; posn[1] = 0.0;
    normal  = new float[2];
    normal[0] = 0.0; normal[1] = 1.0;
    borders[1] = new D2_Plane(ps, posn, normal, -0.7);

    //left wall
    posn = new float[2];
    posn[0] = 0.0; posn[1] = 300.0;
    normal  = new float[2];
    normal[0] = 1.0; normal[1] = 0.0;
    borders[2] = new D2_Plane(ps, posn, normal, -0.7);

    //right wall
    posn = new float[2];
    posn[0] = 600.0; posn[1] = 300.0;
    normal  = new float[2];
    normal[0] = -1.0; normal[1] = 0.0;
    borders[3] = new D2_Plane(ps, posn, normal, -0.7);

    for (int i = 0; i <4 ;i++) {
    	ps->addCollider(borders[i]);
    }

}


bool ParticleField::makeSpring(QMouseEvent * me){
	Particle* p = selectParticle(me);
	if ( p == 0 || clicked == 0 || p == clicked)
		return false;
	Spring* f;
	if(!sprType)
		f = new Spring(p, clicked);
	else {	
		f = new Spring(p, clicked, sprType->getKs(),
			sprType->getKd(), sprType->getLen());
		f->setColor(sprType->getColor());			
	}			
	emit action("made spring");
	ps->addForce(f);
	return true;
}


void ParticleField::addParticle(int x, int y, bool fixed) {

	if (snapToGrid){
		int q = x % gridSize;
		int m = x / gridSize;
		if(q < gridSize - q) x = m*gridSize;
		else x = (m+1)*gridSize;
		
		q = y % gridSize;
		m = y / gridSize;
		if(q < gridSize - q) y = m*gridSize;
		else y = (m+1)*gridSize;
	}
	
	Particle* q = ps->addParticle((float)x,(float) y, 0.0);
	
	if(fixed) {
		// fix the particle in place, 2 diffn ways
    	if(mode == COLLISIONS)
    		q->setFixed(true);
    	else
    	    cs->fixParticle(*q);
	}
}


void ParticleField::boxSelect(QMouseEvent* me) {
	int minx, maxx, miny, maxy, px, py;
	//cout << "entering boxSelect" << endl;
	
	if(startx < me->x()) { minx = startx; maxx = me->x();}
	else { minx = me->x(); maxx = startx;}
	
	if(starty < me->y()) { miny = starty; maxy = me->y();}
	else { miny = me->y(); maxy = starty;}	
	
	Particle* p;
	unsigned int num = selected->size();
	//cout << selected->size();
    for (unsigned int i = 0; i < ps->size(); i++) {
    	p = (*ps)[i];
    	px = (int) p->x[Particle::POSN];
    	py = (int) p->x[Particle::POSN+1];
    	//cout << px<< "  " << py << endl;
    	if (px > minx && px < maxx && py > miny && py < maxy) {
    	    //cout<< "inserting" << endl;
    		selected->insert(p);
    	}	
    }
    if (num != selected->size()|| selected->size() == 0)
    	emit numParticlesSelected(selected->size());
}


// Assumes tracemap has been set to the correct dimensions
void ParticleField::updateTrace() {
	if (tracer && tracing) {
		QPainter p(&tracemap);
		p.setPen(tracePen);
		p.drawLine((int) tracer->x[Particle::POSN],
			(int)tracer->x[Particle::POSN+1],
			(int)tracer->x[Particle::LASTX],
	        (int)tracer->x[Particle::LASTX+1]);
	}
}


void ParticleField::setSelectMode() {
	// box select allowed in Collisions mode from 28-1-00
	/*if (mode == COLLISIONS) {
		emit action("Sorry, box select only in Constraint mode.");
		return;
	} */
	
	mousemode = SELECT;
	emit action("box select mode.");
}


void ParticleField::setSpringMode() {
	mousemode = MAKESPRING;	
	selected->clear();       //these two lines prob not nesc.
	emit numParticlesSelected(0);
	emit action("make spring mode.");
}


ParticleField::MouseMode
ParticleField::getMouseMode() {
	return mousemode;
}


void ParticleField::setCMode(int newMode) {
	if ((int) mode == newMode) {
		if (mode == COLLISIONS)
			emit action("Already in Collisions mode.");
		else
			emit action("Already in Constraints mode.");
		return;
	}
	
	if	(newMode < 0 || newMode > 1){
		std::cout << "invalid parameter to ParticleField::setCMode()." <<
			"\npassed value: " << newMode << std::endl;  	
	    exit(1);
	}
	mode = static_cast<Mode>(newMode);
	
	if (mode == COLLISIONS)  {
		//mousemode = MAKESPRING;
		emit action("Collisions mode - no constraints possible.");
		cs = ps->enableConstraints(false);
	}
	else  {
		cs = ps->enableConstraints(true);
		emit action("Constraints mode - no borders.");	
	}
	clearAll();	
}


ParticleField::Mode
ParticleField::getCMode() {
	return mode;
}


ParticleField::SolverType
ParticleField::getSolverType() {
	return st;
}


int ParticleField::getGridSpacing() {
	return gridSize;
}

void ParticleField::setSolver(int type) {
	if( ((int) st) == type) {
		emit action("Already using that kind of ODE Solver");
		return;
	}
	
	switch(type) {
		case 0 : delete e;
				  e = new Euler(ps);
				  st = EULER;
				  break;
		case 1 : delete e;
				  e = new MidPoint(ps);
				  st = MIDPOINT;
				  break;
		case 2 : delete e;
				 e = new RungeKutta4(ps);
				 st = RK;
				 break;
				
		default: std::cerr << "invalid parameter to ParticleField::setSolver()." <<
			"\npassed value: " << type << std::endl;  	
	    	exit(1);	
	}
}


void ParticleField::runSim(bool on) {
	if(!running && on) {
		timer->start(interval);
		running = true;
		emit simStarted(running);				
	}
	else if(running && !on){
		timer->stop();
		running = false;
		emit simStarted(running);		
	}		
}


void ParticleField::makeConstraint
	(ConstraintType c, float f){
	if(mode != CONSTRAINTS) return;
	Particle* p = 0;
	Circle::Data d(0.0, 0.0, 0.0, 0.0);
	
	selIter i =  selected->begin();
	selIter l = i;
	switch(c) {
		case FIXED : while(i != selected->end()){
		             	cs->fixParticle(*(*i));
		             	i++;
					}
					break;
					
		case HORIZ : while(i != selected->end()) {
		            	cs->addDimensionConstraint(*(*i), 1);
		            	i++;
					}
					break;
					
		case VERT :  while(i != selected->end()) {
		             	cs->addDimensionConstraint(*(*i), 0);
		             	i++;
					}
	                break;
	
	    case CIRCLE :   p = *i;
	    				d.x = p->x[Particle::POSN];
	    				d.y = p->x[Particle::POSN+1];
	    				d.z = 0.0;
	    				d.r = f*f;
						p->x[Particle::POSN+1] += f;
	    				cs->addCircleConstraint(*p, d);
	    				break;
	    				
	    case DISTANCE : if(selected->size() != 2) return;
	    				l++;
	    				cs->addDistConstraint(*(*i),*(*l), f);
	    				break;
	    				
	    case DEF_DISTANCE : if(selected->size() != 2) return;
	    				    l++;
	    				   { Particle* p1 = *i;
	    				    Particle* p2 = *l;
	    				    float dx = p1->x[Particle::POSN] -
	    				    	p2->x[Particle::POSN];
	    				    float dy = p1->x[Particle::POSN+1] -
	    				    	p2->x[Particle::POSN+1];
	    				    float len = sqrtf(dx*dx+dy*dy);
	    				    cs->addDistConstraint(*p1,*p2, len); }
	    					break;
	    default :  std::cerr << "ParticleField::makeConstraint()" <<
	    			" fallen through case !!!" << std::endl;
	    		
	}
	emit action("made constraint");
	QPaintEvent pev(rect());
    paintEvent(&pev);
}


void ParticleField::deleteSelectedParticles(){
    // deal with the Mouse_Spring first.
    S_Iter k =  selected->find(ms->getTarget());
    if(k != selected->end()) ms->releaseParticle();

    // deal with deleting the tracer
    if(tracing) {
		k = selected->find(tracer);
		if( k != selected->end() )toggleTrace();
	}

	S_Iter i = selected->begin();
	S_Iter j = i;
	while (i != selected->end() ){
		ps->removeParticle(*i);
		j = i++;
		selected->erase(j);	
	}
	
	if(!running) {
		QPaintEvent pev(rect());
    	paintEvent(&pev);
    }
}


void ParticleField::toggleTrace() {

	if(tracing) {  // stop tracing
		tracer = 0;
		tracing  = false;
		emit action("tracing stopped");
	}
	else {
	    // see if just one particle is selected
		if (selected->size() == 1){
			drawTrace = true; tracing = true;		
			S_Iter i = selected->begin();
			tracer = *i;
			emit action("tracing started");						
		}
		else
			emit action("select just one particle to trace");
	}
}


void ParticleField::clearTrace() {
	QPainter p(&tracemap);
	p.fillRect(rect(),*bgBrush);
	emit action("trace cleared");
	if(!tracing) drawTrace = false;
}


void ParticleField::save(QDataStream& qds) {
	qds << interval;
	qds << deltaT;
	qds << borders[0]->coeff();
	
	qds << *ps;
}


void ParticleField::load(QDataStream& qds) {
	clearAll();	
	//these few bits the ParticleField takes care of.
	qds >> interval;
	qds >> deltaT;
	float f;
	qds >> f;  setWallRestitution(f);
	
	ps->load(qds, drag, gravity);
	//mousespring deleted by load operation
	ms = new Mouse_Spring(ps);
    ps->addForce(ms);

    cs = ps->getCS();
    	if (cs) mode = CONSTRAINTS;

}


//toggle functionality
void ParticleField::setSnapToGrid() {
	snapToGrid = !snapToGrid;
}


void ParticleField::setDisplayGrid() {
	displayGrid = !displayGrid;
}


void ParticleField::setGridSpacing(int space) {
	if(space < 10) space = 10;
	if(space > 150) space = 150;
	gridSize = space;	
}
