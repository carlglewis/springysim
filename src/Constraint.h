

#ifndef cl_constraint
#define cl_constraint

#include "matrix/Matrix.h"
//#include "matrix/matrix_io.h"
#include "Particle.h"
#include <vector>
#include <qdatastream.h>

typedef Matrix::Vector<float> Vectf ;
typedef Matrix::LMatrix<float> Matrixf;

class ConstraintSystem;
class Particle;
class ParticleSystem;


class Constraint {
 // A constraint has four calculations to make
 // 1.  The value of the constraint function , C
 // 2. The value of the derivative of the constraint
 // 	function, Cdot.
 // 3. Its block in J 4. Its block in Jdot


public:

	Constraint(ConstraintSystem& sys, unsigned int id = 0);
	Constraint(const Constraint& c);
	const Constraint& operator=(const Constraint& c);
	virtual ~Constraint();
	virtual QDataStream& operator<<(QDataStream&) const = 0;
	
	// the main function. places data in C, Cdot,
	// J, Jdot;
	virtual void calculate() = 0;
	
	// returns true if the Constraint contains
	// a pointer to the particle.
	virtual bool usesParticle(const Particle* p) const = 0;
	
	USHORT id();
	void id(USHORT i);	

protected:
	ConstraintSystem & cs;
	USHORT idnum;


};


class Distance : public Constraint {
public:

    Distance(Particle& a,
    	Particle &b, ConstraintSystem& sys,
    	unsigned int id , float dist = 100);
    Distance(Distance &d);
    virtual ~Distance(){}
    virtual QDataStream& operator<<(QDataStream&) const;

    //call this function and get back a brand spanking
    // new Distance pointed to by the argument
    static void buildFromFile(ParticleSystem*, Constraint*&,
		ConstraintSystem&, QDataStream&);
	float length();
    void length(float f);

    void calculate();
	const Particle& getParticle1() const {return p1;}
    const Particle& getParticle2() const {return p2;}
    virtual bool usesParticle(const Particle* p) const;


private:
	const Distance& operator= (const Distance & d);
	Particle & p1;
	Particle & p2;
	float distance;
	
	float c_func();
	float c_func_dot();
	void fill_J();
	void fill_J_dot();
    float U(); //for UGLY!

};


////////////////////////////////////////
// class Circle
// For keeping a particle on a circle (duh!)
////////////////////////////////////////

class Circle : public Constraint {
public:
    class Data;

    Circle(Particle& p1, ConstraintSystem& sys, unsigned int id,
	Data& data);
	virtual ~Circle(){}
	virtual QDataStream& operator<<(QDataStream&) const;
    static void buildFromFile(ParticleSystem*, Constraint*&,
    	ConstraintSystem&, QDataStream&);

	const Data & getData() const ;
    void calculate();

    virtual bool usesParticle(const Particle* pc) const ;


    class Data{
    public:
		Data(float xval, float yval, float zval, float rval);
		Data(const Data & d);
		const Data & operator=(const Data & d);

		float x, y, z, r;
        friend QDataStream& operator<<(QDataStream&,
        	const Data&);
    };

private:

    Data d;
    Particle & p;

};




////////////////////////////////////////
// class Dimension
// For keeping a particle in a given plane
////////////////////////////////////////

class Dimension : public Constraint {
public:
	//Dimension ={0, 1, 2}
	Dimension(Particle& pl, ConstraintSystem& sys,
		unsigned int id, int dimension);
	
	Dimension(const Dimension & f);
	virtual ~Dimension(){}
	
	void calculate();
	virtual bool usesParticle(const Particle* pc) const;
	virtual QDataStream& operator<<(QDataStream&) const;
	static void buildFromFile(ParticleSystem*, Constraint*&,
		ConstraintSystem&, QDataStream&);
	
private:
   
    float posn;
    Particle & p;
    USHORT dim;

    const Dimension& operator= (const Dimension & f);
    float c_func_dot();
    void fill_J();
    void fill_J_dot();
    
};




class ConstraintSystem {

public:

    ConstraintSystem(ParticleSystem & p);
    ~ConstraintSystem();

	friend class Constraint;
	friend class Distance;
	friend class Circle;
	friend class Dimension;
	void addDistConstraint(Particle & p1, Particle& p2, float length);
	void addDimensionConstraint(Particle &p,int dim);
	void addCircleConstraint(Particle& p, Circle::Data& d);
	void fixParticle(Particle &p);
	void clear();
	bool removeParticleRefs(const Particle* p);
	const Constraint * getConstraint(unsigned int index) const ;
	unsigned int size() const ;
	void adjustForces();
 	float u, sum, sum2;
 	typedef std::vector<Constraint*>::iterator Iter;
 	
 	//enumeration for building system from file.
 	enum ConstraintType{END, DISTANCE, CIRCLE, DIMENSION};
 	friend QDataStream& operator<<(QDataStream&,
 		const ConstraintSystem&);
 	void loadFromFile(QDataStream& qds);
private:
	void initialize(bool psChanged);
	

	ParticleSystem & ps;
	std::vector<Constraint*> * constraints;
	Vectf* C;
	Vectf* Cdot;
	Matrixf* J;
	Matrixf* Jt;    // J transpose
	Matrixf* Jdot;
	Matrixf* M;     // mass Matrix
	Matrixf* LHS;
	Vectf* q;      //position vector
	Vectf* qdot;   //velocity vector
	Vectf* Q;      //force vector
	Vectf *lamda;
	Vectf* b;
	Vectf* final;

	float kd;
	float ks;
	
	float* t; //array for Distance to do calculations with
	bool changed;

};

QDataStream& operator<<(QDataStream&,
	const ConstraintSystem::ConstraintType&);
QDataStream& operator>>(QDataStream&,
	ConstraintSystem::ConstraintType&);
QDataStream& operator<<(QDataStream&, const Constraint&);

#endif
