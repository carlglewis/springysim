#include <vector>
#include <iostream>
#include <qdatastream.h>
#include <qstring.h>

#ifndef cl_particle
#define cl_particle
#include "Constraint.h"
#include "Force.h"
#include "Plane.h"

class Particle {

public:
	Particle(int id = 0);
	Particle(Particle& p);
	Particle& operator=(Particle& p);
	~Particle();
	
	float * x;	// all the stuff's in here
	float mass;
	void pickColor();
	void setFixed(bool b);
	bool isFixed() const;
	unsigned int id() const;
	void id(unsigned int i);
	
	friend std::ostream& operator<<(std::ostream& os, const Particle* p);
	friend QDataStream& operator<<(QDataStream& qds, const Particle& p);
	friend QDataStream& operator>>(QDataStream& qds, Particle& p);

	static const unsigned int POSN = 0;
	static const unsigned int VEL = 3;
	static const unsigned int FORCE = 6;
	static const unsigned int LASTX = 9;
	static const unsigned int COLOR = 12;
private:
	static const unsigned int ARRAY_SIZE = 15;
	bool fixed;
	unsigned int idnum;
	


};


#ifndef cl_Force
class Force;
extern void Force::applyForce() const;
#endif

class ConstraintSystem;
class D2_Collider;
class ParticleSystem {
	
public:
	ParticleSystem(int numParticles = 0);
	~ParticleSystem();
	unsigned int size();
	const std::vector<Particle*>* getSystem() const;
	void addForce(Force* f);
	
	// Particle System manages memory
	// for the Colliders placed in it!!!
	void addCollider(D2_Collider*  c);
	void calculateForces();
	void doCollisions();
	
	// create a particle with given position
	// returns a pointer to the new particle
	Particle* addParticle(float x, float y, float z);
	Particle* getParticle (int index)const;
	void clear();
	void clearSprings();
	
	// returns true if particles have been added or
	// removed from the system since the last time
	// this function was called. Adding/removing springs
	// or forces has no effect.
	bool isChanged();
	Particle* operator[](int index) const;
	int numForces() const;
	const Force* getForce(int index) const;
	
	
	// If called with true, returns a pointer
	// to the new ConstraintSystem so that
	// Constraints can be added directly.
	// don't delete the pointer.
	// returns 0 if called with false
	// any previously returned pointers will
	// be invalid.
	ConstraintSystem* enableConstraints(bool b);
	
	//returns a pointer to the existing CS if one
	// is enabled, otherwise returns 0;
	ConstraintSystem* getCS();
	
	void removeParticle(Particle* p);
	
	enum SysElement{
		PARTICLE, FORCE, CONST_FIELD, SPRING,
		GLOBAL_DRAG, DRAG, CONSTANT, CONSTRAINTSYSTEM,
		COLLIDER, D2_PLANE, END};
		
	friend QDataStream& operator<<(QDataStream &,
	const ParticleSystem&);	
	
	// calling this will invaidate ALL pointers
	// which point to stuff owned by the ParticleSystem
	// everything is cleared BEFORE loading from the
	// datastream apart from colliders at present
	void load(QDataStream& qds, Global_Drag*&, Const_Field*&);
	
private:
	std::vector<Particle* > * particles;
	std::vector<Force* > * forces;
	std::vector<D2_Collider* > * colliders;
	ConstraintSystem* cs;
	// used to reset the id number of all
	// the particles if one is deleted.  These
	// numbers are used by the constraint system
	void renumber();
	bool changed;
	void clearAllForces();
	typedef std::vector<Force* >::iterator F_Iter;
	typedef std::vector<Particle*>::iterator P_Iter;
};

//global io

QDataStream& operator<<(QDataStream &,
	const ParticleSystem::SysElement&);
QDataStream& operator>>(QDataStream &,
	ParticleSystem::SysElement&);
	


#endif
