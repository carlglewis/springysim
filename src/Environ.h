
#ifndef cl_environ
#define cl_environ
#include <qstring.h>
#include <iostream>

/*
*	These methods are for dealing with finding the
*	location of the resources that nees to be loaded
*	at runtime.  They all have to be in the directory
*	given by the environment variable SPRING_RESOURCE
*/

#define RESOURCE_DIR "SPRING_RESOURCE"

/**
 * ATM resources all live in one directory, which is defined at compile time	
 * (with configure). The location of this dir can also be given in an
 * environment variable, SPRING_RESOURCE. The env. variable overrides the
 * default location, if set.
 * This function takes the basename of a \p file and puts the full path to the
 * file into \p result, if the file is found in either of these two locations.
 *
 * \p result The absolute path of the file given (including the basename)
 * \return true on success, false on error
 */
bool getResourcePath(QString &result, const QString &file);


/*
*	Prints a standard error message.
*/
void printResourceError();


#endif


