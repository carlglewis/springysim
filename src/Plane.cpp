#include "Plane.h"


D2_Collider::D2_Collider(ParticleSystem* parent) :
	ps(parent){
	
}

D2_Collider::D2_Collider(const D2_Collider& c):
	ps(c.ps){}
	

D2_Collider&
D2_Collider::operator=(const D2_Collider& c) {
	ps = c.ps;
	return *this;
}



// the position argument should be a dynamically allcated
// array of 2 floats [x1, y1].which the plane passes through
// the normal is an array of two floats.


D2_Plane::D2_Plane(ParticleSystem* parent,
	float* position, float* normal, float coeffOfRest) :
	D2_Collider(parent)	{
	if( parent == 0 || position == 0 || normal == 0 ) {
		std::cout << "Plane::Plane() called with null "<< "argument" <<
			std::endl;	
		exit(1);
	}
	if( coeffOfRest >= 0) {
		std::cout << "Plane::Plane : " <<
		"coeffOfReff must be < 0" << std::endl;
	}
    posn = position;
    norm = normal;
    r = coeffOfRest;
}

D2_Plane::~D2_Plane() {
	delete[] posn;
	delete[] norm;
}

D2_Plane::D2_Plane(const D2_Plane& p) :
	D2_Collider(p.ps) {
	norm = new float[2];
	posn = new float[2];
	operator=(p);
}

D2_Plane& D2_Plane::operator=(const D2_Plane& p) {
	if (this != &p) {
		norm[0] = p.norm[0]; norm[1] = p.norm[1];
		posn[0] = p.posn[0]; posn[1] = p.posn[1];
		r = p.r;
	}
	return *this;
}


void D2_Plane::coeff(float val) {
	r = val;
}


float D2_Plane::coeff() const {
   return r;
}



void D2_Plane::solveCollisions() {
   //first test for a collision
   Particle* p;
   float temp[2];
   float vtang[2];
   float outnorm[2];
   outnorm[0] = -1.0 * norm[0];
   outnorm[1] = -1.0 * norm[1];
   float result;
   static int j = Particle::VEL;
   for (unsigned int i = 0; i < ps->size(); i++ ) {
		p = (*ps)[i];		
        temp[0] = p->x[0] - posn[0];
        temp[1] = p->x[1] - posn[1];
        result = (temp[0]) * (norm[0]) + (temp[1])*(norm[1]);
        if (result <= 0) {
        // deal with the response
        // find the normal component of the velocity vector
        	//PR(p->x[0]); PR(p->x[1]);
            //PR(p->x[j]); PR(p->x[j+1]);
         	result =   ( outnorm[0]) * (p->x[j]) +
         		(outnorm[1]) * (p->x[j+1]);
         	if(result < 0.0 ) continue;       	       	
        	temp[0] = result *  (outnorm[0]);
        	temp[1] = result *  (outnorm[1]);
        	//PR(temp[0]); PR(temp[1]);
        	//temp now holds the normal component
        	// of velocity.
        	vtang[0] = p->x[j] - temp[0];
        	vtang[1] = p->x[j+1] - temp[1];
        	//check for contact     	
			result = sqrtf(temp[0] * temp[0] + temp[1]* temp[1]);
        	//PR(result);
        	if (result < 10.0) {
        		temp[0] = 0.0;
        		temp[1] = 0.0;
        	}
        	//PR(vtang[0]); PR(vtang[1]);
        	// vtang is the component of velocity tangent
        	// to the plane.
        	p->x[j] = vtang[0] + (r * temp[0]);
        	p->x[j+1] = vtang[1] + (r * temp[1]);
        	//PR(p->x[Particle::VEL]); PR(p->x[Particle::VEL+1]);
        	//cout << endl;
        }
   }

}
