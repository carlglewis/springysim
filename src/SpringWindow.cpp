/****************************************************************************
** $Id: SpringWindow.cpp,v 1.4 2004/09/13 13:50:59 carl Exp $
**
** Copyright (C) 1992-2000 Troll Tech AS.  All rights reserved.
**
** This file is part of an example program for Qt.  This example
** program may be used, distributed and modified without limitation.
**
*****************************************************************************/

#include "SpringWindow.h"

#include <qimage.h>
#include <qpixmap.h>
#include <qtoolbar.h>
#include <qtoolbutton.h>
#include <qpopupmenu.h>
#include <qmenubar.h>
#include <qkeycode.h>
#include <qmultilineedit.h>
#include <qfile.h>
#include <qfiledialog.h>
#include <qstatusbar.h>
#include <qmessagebox.h>
#include <qprinter.h>
#include <qapplication.h>
#include <qaccel.h>
#include <qtextstream.h>
#include <qpainter.h>
#include <qpaintdevicemetrics.h>
#include <qwhatsthis.h>
#include <qinputdialog.h>
#include "matrix/Matrix.h"
#include <config.h>

#define MAGIC_FILE_NUMBER 666666

SpringWindow::SpringWindow()
    : QMainWindow( 0, "Mass and Spring Simulation",
    WDestructiveClose )
{
    int id;

    //---- File Menu
   QPopupMenu * file = new QPopupMenu( this );
    menuBar()->insertItem( "&File", file );

    file->insertItem( "&New", this, SLOT(newDoc()), CTRL+Key_N );

    id = file->insertItem( /*openIcon,*/ "&Open",
		   this, SLOT(openMenuAction()), CTRL+Key_O );

    id = file->insertItem( /*saveIcon,*/ "&Save",
			   this, SLOT(save()), CTRL+Key_S );

    id = file->insertItem( "Save &as...", this, SLOT(saveAs()) );
    file->insertSeparator();
    file->insertItem( "&Quit", qApp, SLOT( closeAllWindows() ), CTRL+Key_Q );

    pf = new ParticleField( this, "Field" );
    pf->init();
    pf->setFocus();
    setCentralWidget( pf );
    
	connect(pf, SIGNAL(requestReload()),
    		this, SLOT(openFile()));

    //----------  Simulation Tool Bar
    simTools = new SimToolBar( this, "Sim Controls" );
    simTools->setLabel( tr( "Sim Controls" ) );

    connect(simTools, SIGNAL(simModeChanged(bool)),
    	pf, SLOT(runSim(bool)));
    connect(pf, SIGNAL(simStarted(bool)),
    	simTools, SLOT(setButtonOn(bool)));

    connect(simTools, SIGNAL(deltaTChanged(float)),
    	pf, SLOT(setDeltaT(float)));

    connect(simTools, SIGNAL(gravityChanged(int)),
    	pf, SLOT(setGravity(int)));
    	
    connect(simTools, SIGNAL(wallcoeffChanged(float)),
    	pf, SLOT(setWallRestitution(float)));
    	
    connect(simTools, SIGNAL(dragChanged(float)),
    	pf, SLOT(setViscousDrag(float)));	
    	
    connect(simTools, SIGNAL(updateTimeChanged(int)),
    	pf, SLOT(setUpdateTime(int)));
    	
	//-------Spring Tool bar	
	springTools = new SpringToolBar(this, "Spring Tools");
	connect(springTools, SIGNAL(typeAltered(SpringType*)),
	pf, SLOT(setSpringType(SpringType*)));
	springTools->init();
	
	
	//--------Edit Menu
	edit = new QPopupMenu( this );
	menuBar()->insertItem( "&Edit", edit );		
		
	id = edit->insertItem( "&Box Select",
		   pf, SLOT(setSelectMode()), CTRL+Key_B );
	id = edit->insertItem( "&Make Springs",
		   pf, SLOT(setSpringMode()), CTRL+Key_M );	
	connect(edit, SIGNAL(aboutToShow()), this,
		SLOT(updateEditMenu()));
		
	edit->insertSeparator();	
	id = edit->insertItem( "Add Constraint...",
		this, SLOT(showDialog()));			
		
	edit->insertSeparator();

	id = edit->insertItem( "Snap To Grid",
		pf, SLOT(setSnapToGrid()), CTRL+Key_P);
	id = edit->insertItem( "Set Grid Spacing...",
		this, SLOT(gridSize()));
					
	id = edit->insertItem( "Clear &All",
		   pf, SLOT(clearAll()), CTRL+Key_A );
	id = edit->insertItem( "Clear Sp&rings",
		   pf, SLOT(clearSprings()), CTRL+Key_R );
		
	//----- View Menu
	view =  new QPopupMenu( this );
	menuBar()->insertItem( "&View", view );	
	id = view->insertItem( "&Trace Particle",
		pf, SLOT(toggleTrace()), CTRL+Key_T );
	id = view->insertItem( "Clear Trace",
		pf, SLOT(clearTrace()), CTRL+Key_C);	
	view->insertSeparator();
	id = view->insertItem( "Grid",
		pf, SLOT(setDisplayGrid()), CTRL+Key_D);
		 	
		
	// make the constraint chooser dialog
    conDialog = new ConstraintChooser(this, "Make Constraints");
    connect(pf, SIGNAL(numParticlesSelected(unsigned int)),
    	conDialog, SLOT(numParticlesSelected(unsigned int)));
    	
    connect(conDialog, SIGNAL(makeConstraint(ConstraintType, float)),
    	pf, SLOT(makeConstraint(ConstraintType, float)));

	//----- Menu to bring up Dialog and to change modes
	d = new QPopupMenu( this );
    menuBar()->insertItem( "S&im Mode", d );

	id = d->insertItem( "Constraint Mode",
		pf, SLOT(setCMode(int)));
	d->setItemParameter ( id, (int) ParticleField::CONSTRAINTS);
	id = d->insertItem( "Collisions Mode",
		pf, SLOT(setCMode(int)));
	d->setItemParameter ( id, (int) ParticleField::COLLISIONS);
	connect(d, SIGNAL(aboutToShow()), this,
		SLOT(updateSimMenu())); 	
		
	//------Solver Menu
	solvers = new QPopupMenu(this);
	menuBar()->insertItem( "S&olvers", solvers);
	id = solvers->insertItem( "Euler Solver",
		pf, SLOT(setSolver(int)));
	solvers->setItemParameter ( id, (int) ParticleField::EULER);
	id = solvers->insertItem( "MidPoint Method",
		pf, SLOT(setSolver(int)));
	solvers->setItemParameter ( id, (int) ParticleField::MIDPOINT);		
	 id = solvers->insertItem( "Runge-Kutta-4",
		pf, SLOT(setSolver(int)));
	solvers->setItemParameter ( id, (int) ParticleField::RK);
	connect(solvers, SIGNAL(aboutToShow()), this,
		SLOT(updateSolverMenu()));
		
	//------Help Menu
	QPopupMenu * help = new QPopupMenu( this );
	menuBar()->insertSeparator();
	menuBar()->insertItem( "&Help", help );
	
	help->insertItem( "&About", this, SLOT(about()), Key_F1 );
	help->insertItem( "About &Qt", this, SLOT(aboutQt()) );
	help->insertSeparator();
	
	connect(pf, SIGNAL(action(char*)), this, SLOT(message(char*)));
	setCaption("Springy_Sim - Untitled");
	
	//make sure we're big enough
	adjustSize();      //doesnt seem to do much
	// say that we are done
	statusBar()->message( "Ready", 2000 );
}


SpringWindow::~SpringWindow()
{}


void SpringWindow::message(char* s)	{
	QString str(s);
	statusBar()->message( s, 2000 );
}

void SpringWindow::updateSimMenu() {
	ParticleField::Mode m = pf->getCMode();
	if(m == ParticleField::CONSTRAINTS) {
		d->setItemChecked(d->idAt(0), true); 	
		d->setItemChecked(d->idAt(1), false);
	}
	else {
		d->setItemChecked(d->idAt(0), false);
	    d->setItemChecked(d->idAt(1), true);
	}
	
}


void SpringWindow::updateEditMenu() {
	 ParticleField::MouseMode m = pf->getMouseMode();
     if(m == ParticleField::SELECT){
         edit->setItemChecked(edit->idAt(0), true);
         edit->setItemChecked(edit->idAt(1), false);
     }
     else {
     	 edit->setItemChecked(edit->idAt(0), false);
         edit->setItemChecked(edit->idAt(1), true);
     }
}


void SpringWindow::updateSolverMenu() {
	ParticleField::SolverType s = pf->getSolverType();
	if(s ==  ParticleField::EULER) {
		solvers->setItemChecked(solvers->idAt(0), true);
  		solvers->setItemChecked(solvers->idAt(1), false);
  		solvers->setItemChecked(solvers->idAt(2), false);
	}
	else if (s == ParticleField::MIDPOINT){
	    solvers->setItemChecked(solvers->idAt(0), false);
  		solvers->setItemChecked(solvers->idAt(1), true);
  		solvers->setItemChecked(solvers->idAt(2), false);
	}
	else if (s == ParticleField::RK){
	    solvers->setItemChecked(solvers->idAt(0), false);
  		solvers->setItemChecked(solvers->idAt(1), false);
  		solvers->setItemChecked(solvers->idAt(2), true);
	}
}


void SpringWindow::newDoc() {
	QString s;
	s.sprintf( "newDoc called s");
	statusBar()->message( s, 2000 );
	pf->clearAll();
	setCaption("Springy_Sim - Untitled");
}


void 
SpringWindow::openFile()
{
	QFile f(currentFile);
	if(!f.open(IO_ReadOnly)) {
		statusBar()->message("Could not open file", 2000 );
		return;	
	}
	QDataStream q(&f);
	unsigned int magic = MAGIC_FILE_NUMBER;  unsigned int in;
	q >>in;
	if (magic != in) {
		statusBar()->message("Incorrect file format", 2000 );
		return ;
	}
	pf->load(q);
	statusBar()->message("file loaded", 2000 );
	setCaption("Springy_Sim - " + truncateFilename(currentFile));
	SimData sd;
	pf->getToolBarSettings(&sd);
	simTools->setValues(sd);

}

void SpringWindow::openMenuAction()
{
	QString s( QFileDialog::getOpenFileName( QString::null,
	    	"Springy_Sim files (*.spg)", this ) );
    	
	if ( s.isEmpty() )  {
		statusBar()->message("Enter a filename!", 2000 );
		return;
	}
	currentFile = s;
	openFile();
}


void SpringWindow::save()
{
	if(currentFile.isNull()) {
		saveAs();
		return;	
	}
	saveFile(currentFile);	
}


void SpringWindow::saveAs()
{
	QString s;
	QString fileName = QFileDialog::getSaveFileName( "../saves/newfile.spg",
		"Springy_Sim files (*.spg)", this );
	if ( fileName.isNull() ) {
	    	s = "You must enter a filename";
	    	statusBar()->message( s, 2000 );
 	   	return;
	}
	saveFile(fileName);
}


bool SpringWindow::saveFile(QString& fileName){

    QString s;
	QFile f(fileName);
    if(!f.open( IO_WriteOnly )) {
    	s = "Could not open file to write!!";
    	statusBar()->message( s, 2000 );
    	return false;
    }
    QDataStream q( &f );
    unsigned int magic = MAGIC_FILE_NUMBER;
    q << magic;
    pf->save(q);
    f.close();
    statusBar()->message( "Save complete", 2000 );

    currentFile = f.name();
	setCaption("Springy_Sim - " + truncateFilename(currentFile));
	return true;
}



void SpringWindow::about()
{
    QMessageBox::about( this, "About Springysim",
        PACKAGE_STRING " by Carl Lewis\nPlease send bugs, comments and flames to:\n"
        PACKAGE_BUGREPORT);
}


void SpringWindow::aboutQt()
{
    QMessageBox::aboutQt( this, "Qt Kicks Arse!!" );
}


void SpringWindow::showDialog() {
	conDialog->show();
}


void SpringWindow::gridSize() {
	bool ok = false;
	int res = QInputDialog::getInteger("Grid Size",
		"Please enter a whole number", pf->getGridSpacing(),
			10, 150, 2, &ok, this);
	if ( ok ) pf->setGridSpacing(res);
		
}


QString& SpringWindow::truncateFilename(QString& s){
	int pos = s.findRev('/');
	if (pos == -1) return s;
	s.remove(0, pos+1);
	return s;
}



int main( int argc, char **argv ) {

	try {
		QApplication app(argc, argv );
		SpringWindow *s = new SpringWindow();
    	s->show();
    	app.connect( &app, SIGNAL(lastWindowClosed()), &app, SLOT(quit()) );
    	return app.exec();
    }
    catch(Matrix::Error &e) {
	    std::cout<< "Matrix Exception" << std::endl;
	    std::cout << e.message() << std::endl;
    }
 }
