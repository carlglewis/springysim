#include "Mouse_Spring.h"

Mouse_Spring::Mouse_Spring(ParticleSystem *psys) {
	
	p1 = 0;
	ks = 2.2; kd = 3.0;
	ps = psys;
	active = false;
	x = y = 0;
}

Mouse_Spring::~Mouse_Spring() {}

bool Mouse_Spring::isActive() const {
	return active;
}


void Mouse_Spring::pickParticle(int mousex, int mousey) {
	//cout << "Mouse_Spring::pick_Particle()" << endl;
	p1 = 0;
	Particle* p;
	float min = 200000.0; float prog;
	for (unsigned int i = 0; i < ps->size(); i++) {
		p = (*ps)[i];
		prog = pow((p->x[0] - (float)mousex), 2) +
			pow((p->x[1] - (float)mousey), 2);
		if (prog < min) {
			p1 = p; min = prog; 		
		}    				
    	}
	if(!p1) active = false;
	else active = true;
}


void Mouse_Spring::update_posn(int mousex, int mousey){
	
	x = (float) mousex;
	y = (float) mousey;
	//cout << "Mouse_Spring::update_posn()" <<
	//	x << " " << y <<endl;
}


void Mouse_Spring::releaseParticle() {
	active =  false;
}

void Mouse_Spring::applyForce() const
{	
	if (!active)
		return;
	float l[2];

//	std::cout << "Mouse_Spring::applyForce() " << x << " "<< y << std::endl;
	float norml;
	float dotProd, scalar;
	static int v = Particle::VEL;
	//cout << p1 << p2;
	l[0] = p1->x[Particle::POSN] - x;
	l[1] = p1->x[Particle::POSN+1] - y;

	norml = sqrtf( l[0]*l[0] + l[1]*l[1]);

	// 2D only
	dotProd = p1->x[v]*(l[0]) + p1->x[v+1]*(l[1]);

	scalar =  - (ks*(norml - r) + kd*(dotProd / norml)) / norml;
	//cout << "norml : " << norml << endl;
	//cout << "dotProd : " << dotProd << endl;
	//cout << "scalar: " << scalar << endl;
	//cout << "l[i] : " << l[0] << " " <<l[1] << endl << endl;

	p1->x[Particle::FORCE] += scalar * l[0];
	p1->x[Particle::FORCE+1] += scalar * l[1];

//	std::cout << p1 ;
}


