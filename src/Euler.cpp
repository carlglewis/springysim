#include "ODESolver.h"
#include <iostream>

Euler::Euler(ParticleSystem * p) :
	ODESolver(p){
	temp1 = new float[6];
}

Euler::~Euler() {
  delete[] temp1;
}

void Euler::update(float deltaT) {
	//cout << "Euler: entering update" << endl;
	Particle *p;
	ps->calculateForces();
	for (unsigned int i = 0; i < ps->size(); i++) {
	 	p = (*ps)[i];
	 	if (p->isFixed()) continue;
	 	  //cout << "derivative vector" << endl;
		// get the "derivative vector" and scale it.
	 	temp1[0] = deltaT * (p->x[Particle::VEL]);
	 	temp1[1] = deltaT * (p->x[Particle::VEL+1]);
	 	temp1[2] = deltaT * (p->x[Particle::VEL+2]);
	 	temp1[3] = deltaT * (p->x[Particle::FORCE] / p->mass);
		temp1[4] = deltaT * (p->x[Particle::FORCE+1] / p->mass);
		temp1[5] = deltaT * (p->x[Particle::FORCE+2] / p->mass);

		
		//cout << "setting LastX" << endl;
	 	p->x[Particle::LASTX] = p->x[0];
	 	p->x[Particle::LASTX+1] = p->x[1];
	 	p->x[Particle::LASTX+2] = p->x[2];
	 	
	 	p->x[0] += temp1[0];
	 	p->x[1] += temp1[1];
	 	p->x[2] += temp1[2];
		p->x[Particle::VEL] += temp1[3];
		p->x[Particle::VEL+1] += temp1[4];
		p->x[Particle::VEL+2] += temp1[5];
	}
	
	ps->doCollisions();
    //cout << "Euler: leaving update" << endl;
}

