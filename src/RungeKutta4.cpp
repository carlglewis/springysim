#include "ODESolver.h"
#include <iostream>

const float RungeKutta4::factor[] = {0.5, 1.0};

RungeKutta4::RungeKutta4(ParticleSystem * p) :
	ODESolver(p){
		
	if( (num_pcls = ps->size()) > 0) {
		yn = new float[3*num_pcls];
		result = new float[6*num_pcls];
	}			
	else {
		yn = 0;
		result = 0;
	}
		
	yn_size = num_pcls;	
	sixth = 1.0 / 6.0;
}

RungeKutta4::~RungeKutta4() {
	delete[] yn;
	delete[] result;
}

void RungeKutta4::update(float deltaT) {
	//cout << "RungeKutta4: entering update" << endl;
	
	// make sure yn and k1 is big enough
	if( (num_pcls = ps->size()) == 0) return;
	if (num_pcls > yn_size) {
		yn_size = num_pcls;
		delete [] yn; delete [] result;
		yn = new float[3*yn_size];
		result = new float[6*num_pcls];
	}
	
	// save the original  velocity
	// values in yn
	Particle *p;
	unsigned int j, k; j = 0;
	for(unsigned int i = 0; i < num_pcls; i++) {
		p = (*ps)[i];	
		for(k = 0; k < 3; k++)
	    	yn[j+k] = p->x[Particle::VEL+k];
	 	j+=3;
	 	
	 	//keep the previous position here
	 	for(k = 0; k < 3; k++)
	 		p->x[Particle::LASTX+k] = p->x[k];
	
	}
	
	// do the first Euler bit to
	halfDel = deltaT / 2.0;
	ps->calculateForces();
	float a;
	for (unsigned int i = 0; i < num_pcls; i++) {
	 	p = (*ps)[i];
	 	if (p->isFixed()) continue;
	 	  //cout << "derivative vector" << endl;
		// get the "derivative vector" and scale it.
		
		for (int j = 0 ; j < 3 ;j++) {
		    result[i*6+j] = deltaT*(p->x[Particle::VEL+j]);
		    p->x[Particle::POSN+j] +=
		    	(halfDel*(p->x[Particle::VEL+j]));
		}
	 		
	 	for (int j = 0 ; j < 3 ;j++){
	 		a = (p->x[Particle::FORCE+j])/ p->mass;
	 		result[i*6+j+3] = deltaT*a;
		    p->x[Particle::VEL+j] += (halfDel * a );	
	 			
	 	}
	}	
	ps->calculateForces();
	for( unsigned int k = 0; k < 2; k++) {
		for (unsigned int i = 0; i < num_pcls; i++) {
		    p = (*ps)[i];
		    if (p->isFixed()) continue;
			for (int j = 0 ; j < 3 ;j++) {
				result[i*6+j] += 2*deltaT*(p->x[Particle::VEL+j]);
		    	p->x[Particle::POSN+j] = (p->x[Particle::LASTX+j])+
		    		(factor[k]*deltaT*(p->x[Particle::VEL+j]));
			}
			
			for (int j = 0 ; j < 3 ;j++){
	 			a = deltaT * ((p->x[Particle::FORCE+j])/ p->mass);
	 			result[i*6+j+3] += 2*a;
		    	p->x[Particle::VEL+j] = (yn[(i*3)+j])+(factor[k] * a );		
	 		}
		}
		ps->calculateForces();
	}
	
	for (unsigned int i = 0; i < num_pcls; i++) {
	 	p = (*ps)[i];
	 	if (p->isFixed()) continue;
	 	  //cout << "derivative vector" << endl;
		// get the "derivative vector" and scale it.
		
		for (int j = 0 ; j < 3 ;j++) {
		    result[i*6+j] += deltaT*(p->x[Particle::VEL+j]);
		}
	 		
	 	for (int j = 0 ; j < 3 ;j++){
	 		a = (p->x[Particle::FORCE+j])/ p->mass;
	 		result[i*6+j+3] += deltaT*a;		 			
	 	}
	}	
	
	for (unsigned int i = 0; i < num_pcls; i++) {
		p = (*ps)[i];
		if (p->isFixed()) continue;
		for (int j = 0 ; j < 3 ;j++)
			p->x[Particle::POSN+j] =
				(p->x[Particle::LASTX+j] + (sixth*(result[i*6+j])));
		
		for (int j = 0 ; j < 3 ;j++)
			p->x[Particle::VEL+j] =
				(yn[(i*3)+j]) + (sixth*(result[i*6+j+3]));				
	}
	
	
    //cout << "RungeKutta4: leaving update" << endl;
}


