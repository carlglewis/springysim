#include <qspinbox.h>

#ifndef CL_CUSTOM_SPINBOX
#define CL_CUSTOM_SPINBOX


class ZeroToOne : public QSpinBox {
  Q_OBJECT
public:
   ZeroToOne(QWidget * parent = 0, const char * name = 0 );

signals:
    void valueChangedf(float value);

public slots:
    void notify(int val);
    void setValuef(float val);

protected:
  QString mapValueToText( int value );
  int mapTextToValue( bool* ok );


};



class ZeroTo500 : public QSpinBox {
	Q_OBJECT
public:
	ZeroTo500(QWidget * parent = 0, const char * name = 0);
	
signals:
    void valueChangedf(float value);

public slots:
    void notify(int val);
    void setValuef(float val);

protected:
  QString mapValueToText( int value );
  int mapTextToValue( bool* ok );	

};


class ZeroToTen : public QSpinBox {
	Q_OBJECT
	
public:
	ZeroToTen(QWidget * parent = 0, const char * name = 0);
	
signals:
    void valueChangedf(float value);

public slots:
    void notify(int val);
    void setValuef(float val);

protected:
  QString mapValueToText( int value );
  int mapTextToValue( bool* ok );	
};




#endif


