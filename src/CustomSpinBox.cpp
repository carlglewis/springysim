
#include "CustomSpinBox.h"


//----------------------------------
// ZeroToOne Methods
//----------------------------------

ZeroToOne::ZeroToOne(QWidget *parent, const char *name) :
    QSpinBox(parent, name){
    connect(this, SIGNAL(valueChanged(int)), this, SLOT(notify(int)));
}


QString ZeroToOne::mapValueToText(int value) {
    float f = (float) value;
    return QString("%1").arg(f/100);
}


int ZeroToOne::mapTextToValue(bool*ok) {
	return int(text().toFloat()*100);
}


void ZeroToOne::notify(int val) {
    bool b = true;
    emit valueChangedf( ((float)mapTextToValue(&b))/100 );
}


void ZeroToOne::setValuef(float val) {
	float ok;
	if(val < 0.0) ok = 0;
	else if(val > 1.0) ok = 1.0;
	else ok = val;
	setValue((int) (ok * 100.0));
	updateDisplay();
}

//----------------------------------
// ZeroTo500 Methods
//----------------------------------

ZeroTo500::ZeroTo500(QWidget *parent, const char *name):
	QSpinBox(parent, name){
	connect(this, SIGNAL(valueChanged(int)),
		this, SLOT(notify(int)));	
}
	

void ZeroTo500::setValuef(float val) {
	float ok;
	if(val < 0.0) ok = 0;
	else if(val > 500.0) ok = 500.0;
	else ok = val;
	setValue((int) (ok / 5.0));
	updateDisplay();
}


QString ZeroTo500::mapValueToText(int value) {
    float f = (float) value;
    return QString("%1").arg(f*5);
}


int ZeroTo500::mapTextToValue(bool*ok) {
	return int(text().toFloat()/5.0);
}


void ZeroTo500::notify(int val) {
    bool b = true;
    emit valueChangedf( ((float)mapTextToValue(&b))*5.0 );
}



//----------------------------------
// ZeroToTen Methods
//----------------------------------

ZeroToTen::ZeroToTen(QWidget *parent, const char *name):
	QSpinBox(parent, name){
	connect(this, SIGNAL(valueChanged(int)),
		this, SLOT(notify(int)));	
}

	
void ZeroToTen::setValuef(float val) {
	float ok;
	if(val < 0.0) ok = 0.0;
	else if(val > 10.0) ok = 10.0;
	else ok = val;
	setValue((int) (ok * 10.0));
	updateDisplay();
}


QString ZeroToTen::mapValueToText(int value) {
    float f = (float) value;
    return QString("%1").arg(f / 10.0);
}


int ZeroToTen::mapTextToValue(bool*ok) {
	return int(text().toFloat() * 10.0);
}


void ZeroToTen::notify(int val) {
    bool b = true;
    emit valueChangedf( ((float)mapTextToValue(&b)) / 10.0 );
}



