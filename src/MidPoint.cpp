#include "ODESolver.h"
#include <iostream>

MidPoint::MidPoint(ParticleSystem * p) :
	ODESolver(p){
		
	if( (num_pcls = ps->size()) > 0)
		yn = new float[3*num_pcls];		
	else
		yn = 0;
		
	yn_size = num_pcls;	
}

MidPoint::~MidPoint() {
	delete[] yn;
}

void MidPoint::update(float deltaT) {
	//cout << "MidPoint: entering update" << endl;
	
	// make sure yn is big enough
	if( (num_pcls = ps->size()) == 0) return;
	if (num_pcls > yn_size) {
		yn_size = num_pcls;
		delete [] yn;
		yn = new float[3*yn_size];
	}
	
	// save the original  velocity
	// values in yn
	Particle *p;
	unsigned int j, k; j = 0;
	for(unsigned int i = 0; i < num_pcls; i++) {
		p = (*ps)[i];	
		for(k = 0; k < 3; k++)
	    	yn[j+k] = p->x[Particle::VEL+k];
	 	j+=3;
	 	
	 	//keep the previous position here
	 	for(k = 0; k < 3; k++)
	 		p->x[Particle::LASTX+k] = p->x[k];
	
	}
	
	// do the first Euler bit to move the sim
	// to the midpoint.
	halfDel = deltaT / 2.0;
	ps->calculateForces();
	for (unsigned int i = 0; i < ps->size(); i++) {
	 	p = (*ps)[i];
	 	if (p->isFixed()) continue;
	 	  //cout << "derivative vector" << endl;
		// get the "derivative vector" and scale it.
		
		for (int j = 0 ; j < 3 ;j++)
		    p->x[Particle::POSN+j] +=
		    	(halfDel*(p->x[Particle::VEL+j]));
	 		
	 	for (int j = 0 ; j < 3 ;j++)
		    p->x[Particle::VEL+j] +=
		    	(halfDel * ((p->x[Particle::FORCE+j])/ p->mass) );	
	 		
	 	
	}
	ps->doCollisions();
	ps->calculateForces();
	
	for (unsigned int i = 0; i < ps->size(); i++) {
		p = (*ps)[i];
		
		for (int j = 0 ; j < 3 ;j++)
			p->x[Particle::POSN+j] =
				(p->x[Particle::LASTX+j] + (deltaT*(p->x[Particle::VEL+j])));
		
		for (int j = 0 ; j < 3 ;j++)
			p->x[Particle::VEL+j] =
				(yn[(i*3)+j]) + (deltaT * ((p->x[Particle::FORCE+j])/ p->mass) );				
	}
	
	ps->doCollisions();
    //cout << "MidPoint: leaving update" << endl;
}

