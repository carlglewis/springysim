
#include <math.h>
#include <qcolor.h>
#include <qdatastream.h>

#ifndef cl_Force
#define cl_Force

class Particle;
class ParticleSystem;

class Force {

public:
	Force(){}
	virtual ~Force(){}
	virtual void applyForce() const {}
	//overide this function if the Force contains
	// a pointer or reference to a particle.
	virtual bool usesParticle(const Particle*) const {
	 	return false;
	}
    virtual QDataStream& operator<<(QDataStream& qds){
    	return qds;
    }
};


class Const_Field: public Force {
  ParticleSystem *ps;
  float* direction;
  bool active;

public:
  Const_Field(ParticleSystem* p, float x = 0.0,
  	float y = 1.0, float z = 0.0);
  //build object from file.
  Const_Field(ParticleSystem* p, QDataStream& qds);	
  	
  Const_Field(const Const_Field & f);
  Const_Field& operator=(const Const_Field & f);
  ~Const_Field() { delete[] direction; }
  bool isActive() const;
  void activation(bool b);
  virtual void applyForce() const;
  inline void setY(float val) { direction[1] = val; }
  inline float getY() const { return direction[1]; }
  virtual QDataStream& operator<<(QDataStream& qds);

};


class Spring : public Force {

protected:
	Particle* p1;
	Particle* p2;
	float ks;	// spring constant
	float kd;	// damping constant
	float r;		// rest length;
	QColor col;
	Spring(){}    // for subclasses

public:
	Spring(Particle* first, Particle* second,
		float springConstant = 0.4, float dampingFactor = 0.2,
		float restLength = 100.0);
	Spring(ParticleSystem*,QDataStream& qds);
	Spring(const Spring& s);
	Spring& operator=(const Spring& s );
	~Spring(){}
				
	void setSpringConstant(float val);
	float getSpringConstant() const ;
	void setDampingFactor(float val);
	float getDampingFactor() const;
    void setrestLength(float val);
	float getrestLength()const ;
	const Particle* getParticle1() const{ return p1; }
	const Particle* getParticle2() const { return p2; }
	virtual void applyForce() const;
	
	const QColor& getColor() const ;
	void setColor(const QColor &);
	virtual bool usesParticle(const Particle*) const;
	virtual QDataStream& operator<<(QDataStream& qds);
	
};


class Global_Drag : public Force {

    float coeff;
    ParticleSystem* ps;

public:
	Global_Drag(ParticleSystem* p, float coeffOfDrag = 0.08);
	Global_Drag(ParticleSystem*,QDataStream& qds);
	inline float getCoeff() const { return coeff; }
	inline void setCoeff(float val) { coeff = val; }
	virtual void applyForce() const ;
	virtual QDataStream& operator<<(QDataStream& qds);
};


class Drag : public Force {

	Particle* p;
	float coeff;
	
public:
   	Drag(Particle* target, float coeffOfDrag  = 0.02);
   	Drag(ParticleSystem*,QDataStream& qds);
   	float getCoeff() const;
	void setCoeff(float val);
	virtual void applyForce() const;
	virtual bool usesParticle(const Particle*) const;
	virtual QDataStream& operator<<(QDataStream& qds);
};


class Constant : public Force {
	
	Particle* p;
	float f[3];
	
public:
	Constant(Particle* target, float xval = 2.0f,
		float yval = 0.0f, float zval = 0.0f);
	Constant(ParticleSystem*,QDataStream& qds);	
	float getVal(int whichval) const;
	void setVal(float newval, int whichval);
	virtual void applyForce() const;
    virtual bool usesParticle(const Particle*) const;
    virtual QDataStream& operator<<(QDataStream& qds);
};


//global
QDataStream& operator<<(QDataStream& qds, Force& f);

#include "Particle.h"
#endif //cl_Force


