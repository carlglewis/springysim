
#ifndef cl_SOLVER
#define cl_SOLVER

#include "Particle.h"

class ODESolver {

public:
	ODESolver(ParticleSystem * sys ) :
		ps(sys){}
	virtual ~ODESolver(){}
 	virtual void update(float deltaT) = 0;		
	
protected:
	ParticleSystem *ps;	
};


class Euler : public ODESolver {

public:
	Euler(ParticleSystem * sys );
	virtual ~Euler();
 	virtual void update(float deltaT);
 	
private:
	float *temp1;
};


class MidPoint : public ODESolver {

public:
	MidPoint(ParticleSystem * sys );
	virtual ~MidPoint();
 	virtual void update(float deltaT);
 	
private:
	float* yn;
	unsigned int num_pcls, yn_size;
	float halfDel;
};


class RungeKutta4 : public ODESolver {
	
public:
	RungeKutta4(ParticleSystem * sys );
	virtual ~RungeKutta4();
	virtual void update(float deltaT);
	
private:
     float halfDel, sixth;
     unsigned int num_pcls, yn_size;
     float *result, *yn;
     static const float factor[];
};


#endif



