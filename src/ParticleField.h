
#ifndef cl_particleField
#define cl_particleField

#include <iostream>
#include <set>

#include <qtimer.h>
#include <qpixmap.h>
#include <qpoint.h>
#include <qwidget.h>

#include "Particle.h"
#include "Force.h"
#include "ODESolver.h"
#include "Plane.h"
#include "Mouse_Spring.h"
#include "ConstraintChooser.h"
#include "SpringType.h"
#include "SimData.h"

typedef std::set<Particle*>::iterator selIter;

class ParticleField : public QWidget
{
    Q_OBJECT
public:
    ParticleField( QWidget *parent=0, const char *name=0 );
    ~ParticleField();
    //QSizePolicy sizePolicy() const;
    void init();

    enum Mode{ COLLISIONS, CONSTRAINTS};
    enum MouseMode { MAKESPRING, SELECT};
    enum SolverType { EULER = 0, MIDPOINT = 1, RK = 2 };

    MouseMode getMouseMode();
    Mode getCMode();
    SolverType getSolverType();
    int getGridSpacing();

signals:
	// connected to the Window, displays messages on the
	// message bar
	void action(char*);
	
	// connected to the constraintchooser dialog, so it
	// can display the correct sorts of constraints
	// depending on the number of particles selected.
	void numParticlesSelected(unsigned int n);
	
	// connected to the simToolBar, so that the GO button
	// will change state when sim is started from the keyboard.
	void simStarted(bool on);

	/**
	 * This is emitted when the simulation has crashed, 
	 * and the simulation file (if there is one) should
	 * be reloaded.
	 */
	void requestReload();

public slots:
	void update();
	void setDeltaT(float val);
	
	void clearAll();
	void clearSprings();
	
	void setSelectMode();
	void setSpringMode();
	void setCMode(int newMode);
	void setSolver(int type);
	void runSim(bool on);
	
	void setWallRestitution(float val);
	void setGravity(int val);
	void setViscousDrag(float val);
	void setUpdateTime(int);
	void setSpringType(SpringType*);
	void getToolBarSettings(SimData*);
	void makeConstraint(ConstraintType, float);
	void deleteSelectedParticles();
	
	void toggleTrace();
	void clearTrace();
	
	void save(QDataStream&);
	void load(QDataStream&);
	
	void setSnapToGrid();
	void setDisplayGrid();
	void setGridSpacing(int);
	
protected:
	void paintEvent( QPaintEvent * );
	virtual void mousePressEvent ( QMouseEvent * );
	virtual void mouseReleaseEvent ( QMouseEvent * );
	virtual void mouseMoveEvent( QMouseEvent *me );
	virtual void keyPressEvent ( QKeyEvent * ke );
private:

	Particle* selectParticle( QMouseEvent * );
	void drawParticles(QPainter &);
	void drawSprings(QPainter &);
	void drawSpringMatt(QPainter& paint, float pZeroX ,
	float pZeroY, float pOneX, float pOneY);
	void drawBox(QPainter&);
	void drawMouseArt(QPainter&);
	void drawConstraints(QPainter& );
	void drawGrid(QPainter&);
	
	void buildBorders();
	bool makeSpring(QMouseEvent * me);
	void addParticle(int x, int y, bool fixed);
	
	// put all the particles within the box
	// in the selected set.
	void boxSelect(QMouseEvent *);
	
	// plot another point in the trace
	void updateTrace();
	
	ParticleSystem* ps;
	ODESolver* e;
	ConstraintSystem * cs;
	Particle* clicked;
	float deltaT;
	bool leftButtonPressed;
	SpringType* sprType;
	
	bool running;
	QTimer* timer;
	int interval;
	
	D2_Plane* borders[4];
	Global_Drag* drag;
	Const_Field* gravity;
	
	Mouse_Spring* ms;
	std::set<Particle*> *selected;
	typedef std::set<Particle*>::iterator S_Iter;
	Mode mode;
	MouseMode mousemode;
	SolverType st;
	int startx, starty, thisx, thisy;
	
	
	QColor* bg;
	QBrush* bgBrush;
	QPixmap pix;
	
	// to do the tracing
	Particle* tracer;
	QPixmap tracemap;
	bool tracing, drawTrace;
	QColor tracePen;
	
	//for snap particle to grid functionality
	bool snapToGrid, displayGrid;
	int gridSize;
	
	bool drawComplexSpring;
};


#endif //cl_particleField
