#include "SpringType.h"
#include "qcolordialog.h"

//------------------------------------
//  ColorChit Methods
//------------------------------------


ColorChit::ColorChit(QWidget * parent, const char * name):
	QWidget(parent, name), c(0, 0, 0){
	
	setFixedSize(40, 20);
	setBackgroundColor(c);	
}


const QColor& ColorChit::color() const {
	return c;
}


void ColorChit::color(const QColor& color) {
	c = color;
	setBackgroundColor(c);	
}


void ColorChit::mouseDoubleClickEvent(QMouseEvent* me){
	QColor col = c;
	QColor newcol = QColorDialog::getColor(col, this, "colorDialog");
	if(newcol.isValid() && (newcol != c)) {
		c = newcol;
		setBackgroundColor(c);	
		emit colorChanged(c);
	}
}



//---------------------------------------
//  SpringToolBar Methods
//---------------------------------------

SpringToolBar::SpringToolBar(QMainWindow * parent, const char * name):
	QToolBar(parent, name){
	
	newtype = new QPushButton("New Type", this, "newTypeButton");
	cb = new QComboBox(true, this, "spring type list");
	cb->setMinimumWidth(150);
	cb->setInsertionPolicy(QComboBox::AtCurrent);
	a = new QLabel("   ks:", this, "a");
	ks = new ZeroToTen(this, "ksSpinBox");
	
	b = new QLabel("   kd:", this, "b");
	kd = new ZeroToOne(this, "kdSpinBox");
	
	c = new QLabel("   length:", this, "c");
	rl = new ZeroTo500(this, "");
	
	addSeparator();
	chit = new ColorChit(this, "chitbaby");
	addSeparator();
		
	newTypeCount = 0;
	types = new std::list<SpringType*>();
	current = 0;
	loadDefaults();
	
	connect(cb, SIGNAL(activated(int)), this, SLOT(typeChanged(int)));
	connect(ks, SIGNAL(valueChangedf(float)), this,
		SLOT(ksChanged(float))); 	
	connect(kd, SIGNAL(valueChangedf(float)), this,
		SLOT(kdChanged(float)));
	connect(rl, SIGNAL(valueChangedf(float)), this,
		SLOT(lenChanged(float))); 	
	connect(chit, SIGNAL(colorChanged(QColor&)), this,
		SLOT(colorChanged(QColor&)));
	connect(newtype, SIGNAL(clicked()), this,
		SLOT(newSpringType()));		
		
}


SpringToolBar::~SpringToolBar(){
	Iter i = types->begin();
	while(i != types->end()) {
		delete *i; i++;	
	}
    delete types;
}


void SpringToolBar::init() {
	if (! types->empty())
		typeChanged(0);
}


void SpringToolBar::loadDefaults() {

	QString filename;
	if( !getResourcePath(filename, SPRING_TYPE_FILE) ) {   // !!! FATAL ERROR !!!
		std::cerr << "Required file " << filename.latin1() << " not found, "
			<< "exiting." << std::endl;
		exit(1);
	}

    QFile f(filename);

	if(!f.open(IO_ReadOnly)) { 
		std::cerr << "Could not open " << (const char *)filename
			<< "for reading,  exiting." << std::endl;
		exit(1);
	}
	SpringType* st;
	QDataStream data( &f );
	while(!data.atEnd()){
		st = new SpringType();
		data >> (*st);
		types->push_back(st);
		//cout << (*st);
		cb->insertItem(st->getName());
	}

}


void SpringToolBar::typeChanged(int index) {

	//cout << "Entering SpringToolBar::typeChanged()" << endl;
	QString s = cb->currentText();
	Iter i = types->begin();
	while(i != types->end()) {
		if((*i)->getName() == s) break;
		i++;
	}
	if(i == types->end()) {
		//cout << "error in SpringToolBar::typeChanged() " <<endl;
		//exit(1);
		nameChanged(index, s);
		return;
	}
	if (current == *i) return; //just picked the same one
	current = *i;
	ks->setValuef(current->getKs());
	kd->setValuef(current->getKd());
	rl->setValuef(current->getLen());
	chit->color(current->getColor());
	
	emit typeAltered(current);
	
	//cout << "Leaving SpringToolBar::typeChanged()" << endl;
}

void SpringToolBar::ksChanged(float val){
	if(current) current->setKs(val);
}


void SpringToolBar::kdChanged(float val){
	if(current) current->setKd(val);
}


void SpringToolBar::lenChanged(float val){
	if(current) current->setLen(val);
}


void SpringToolBar::colorChanged(QColor& col){
	if(current) current->setColor(col);
}


//called if the user has edited one of the SpringType
//names in the combo box
void SpringToolBar::nameChanged(int pos, const QString& s){
	//cout << "Entering SpringToolBar::nameChanged()" << endl;
    Iter i = types->begin();
	for(int j = 0; j < pos; j++) i++;
	current = *i;
	current->setName(s);	
}


void SpringToolBar::newSpringType() {
	QString a = "New Type";
	QString b; b.setNum(newTypeCount++);
	a += b;
	SpringType* st = new SpringType(a, current->getColor(),
		current->getKs(), current->getKd(), current->getLen());
	types->push_back(st);
	current = st;
	cb->insertItem(a);	
	cb->setCurrentItem((cb->count()) - 1);
	emit typeAltered(current);
	// dont emit signal typeAltered because all values have
	//changed, it will be emited once something other than
	// the name has changed - emitten by typeChanged()		
}
