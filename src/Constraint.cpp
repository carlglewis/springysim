#include "Constraint.h"
#include "matrix/matrix_io.h"
#include <math.h>
#include <iostream>

//#define CONSTRAINT_DB

#ifndef PR
#define PR(x) std::cout << #x " = "<< std::endl << (x) << endl
#endif
////////////////////////////////
// Constraint Methods
/////////////////////////////////

Constraint::Constraint(ConstraintSystem& sys, unsigned int id)
: cs(sys), idnum(id) {}

Constraint::Constraint(const Constraint& c) :
	cs(c.cs), idnum(c.idnum) {}
	
const Constraint&
Constraint::operator=(const Constraint & c){
	if(this != &c) idnum = c.idnum;
	return *this;
}

Constraint::~Constraint(){}

inline USHORT Constraint::id() {
	return idnum;
}

inline void Constraint::id(USHORT i) {
	idnum = i;
}




/////////////////////////////////////
// Distance Methods
/////////////////////////////////////

Distance::Distance(Particle& a,Particle& b,
	ConstraintSystem & sys, unsigned int id, float dist) :
	Constraint(sys, id), p1(a), p2(b),  distance(dist) {
	distance = dist;
}

	
Distance::Distance(Distance & d):
    Constraint(d.cs, d.idnum),p1(d.p1),
	p2(d.p2), distance(d.distance) {}
	
	
const Distance& Distance::operator=(const Distance & d) {
	return *this;
}


QDataStream& Distance:: operator<<(QDataStream& qds) const {
	qds << ConstraintSystem::DISTANCE;
	qds << p1.id() << p2.id() << idnum << distance;
	return qds;				
}

void Distance::buildFromFile(ParticleSystem* ps, Constraint*& d,
	ConstraintSystem& sys, QDataStream& qds){
	Particle *pcl1, *pcl2; USHORT id; float dist;
	int i; qds >> i;
	pcl1 = ps->getParticle(i); qds >> i;
	pcl2 = ps->getParticle(i);
	qds >> id; qds >> dist;
	d = new Distance(*pcl1, *pcl2, sys, id, dist);   	
}



inline float Distance::length() {	
	return distance;
}

inline void Distance::length(float f) {
	distance = f;
}


float Distance::U() {
	
	cs.sum2 = ((cs.t[0])*(cs.t[0])
		+ (cs.t[1])*(cs.t[1])
		+ (cs.t[2])*(cs.t[2]));
	return sqrt(cs.sum2);
}

float Distance::c_func() {
	return ( cs.u - distance);
}
	

float Distance::c_func_dot() {

	cs.t[6] = (cs.t[0])*(cs.t[3]);
	cs.t[7] = (cs.t[1])*(cs.t[4]);
	cs.t[8] = (cs.t[2])*(cs.t[5]);
	cs.sum = (cs.t[6]) + (cs.t[7])+ (cs.t[8]);
	return (cs.sum / cs.u);
}

void Distance::fill_J() {
	//cout << "entering Distance::fill_J()" << std::endl;
	//cout << "entering Distance::fill_J()" << std::endl;
	// all the results are put in the same numbered
	// row as the idnum.  They are put in thec ol
	// corresponding to the particles id.
	//std::cout << "*(cs.J): \n"<<*(cs.J) << std::endl;
	try{
		for(USHORT i = 0; i < 3;i++) {
			float val = (cs.t[i])/ (cs.u);
			cs.J->setElem(val, idnum, (p1.id()*3)+i);
			cs.J->setElem(-val, idnum, (p2.id()*3)+i);
		}
	}
	catch(Matrix::Error &e)  {
		std::cout <<"Matrix Exception encountered" <<std::endl;
		std::cout << e.message()<<std::endl;
		exit(1);	
	}
	//std::cout << "entering Distance::fill_J()" << std::endl;
}

void Distance::fill_J_dot() {
    //std::cout << "entering Distance::fill_J_dot()" << std::endl;
	float val1, val2, val3, val4;
	for(USHORT i = 0; i < 3;i++) {
		 val1 = (cs.t[i+3]) / cs.u;
		 val2 = (cs.t[i])*(cs.sum);
		 val3 = val2 /(pow(cs.sum2, 1.5));
		 val4 = val1 - val3;
		
		cs.Jdot->setElem(val4, idnum, (p1.id()*3)+i);
		cs.Jdot->setElem(-val4, idnum, (p2.id()*3)+i);
	}
	 //std::cout << "leaving Distance::fill_J_dot()" << std::endl;	
}

void Distance::calculate() {

	//std::cout << "entering Distance::calculate()" << std::endl;
	static int pos = Particle::POSN;
	static int vel = Particle::VEL;
	cs.t[0] = (p1.x[pos] - p2.x[pos]);
	cs.t[1] = (p1.x[pos+1] - p2.x[pos+1]);
	cs.t[2] = (p1.x[pos+2] - p2.x[pos+2]);
	cs.t[3] = p1.x[vel] - p2.x[vel];
	cs.t[4] = p1.x[vel+1] - p2.x[vel+1];
	cs.t[5] = p1.x[vel+2] - p2.x[vel+2];
	cs.u = U(); // bit that goes on the bottom
	//cout << "calulate: cs.u= " << cs.u << endl;
	
	
	cs.C->setElem(c_func(),idnum);
	cs.Cdot->setElem(c_func_dot(), idnum);
#ifdef CONSTRAINT_DB	
	PR(*(cs.C)); PR(*(cs.Cdot));
#endif	
	fill_J();
	fill_J_dot();	

}


bool Distance::usesParticle(const Particle* p)const {
	if(p == &p1 || p == &p2) return true;
	else return false;
}

/////////////////////////////////
// Methods for class Circle
// and class Circle::Data (just a data struct thing
/////////////////////////////////


Circle::Data::Data(float xval, float yval, float zval, float rval):
    x(xval), y(yval), z(zval), r(rval*rval)
{}


Circle::Data::Data(const Data & d):
    x(d.x), y(d.y), z(d.z), r(d.r){}


const Circle::Data & Circle::Data::operator=(const Data & d){
    if (this != &d) {
	x=d.x; y=d.y; z=d.z; r=d.r;
    }
	return *this;

}

Circle::Circle(Particle &p1, ConstraintSystem& sys, unsigned int id,
	Data& data): Constraint(sys, id) ,d(data), p(p1){}


QDataStream& Circle::operator<<(QDataStream& qds) const	{
	qds << ConstraintSystem::CIRCLE;
	qds << p.id() << idnum;
	qds << d.x << d.y << d.z << sqrtf(d.r);
	return qds;	
}


void Circle::buildFromFile(ParticleSystem* ps, Constraint*& pc,
    ConstraintSystem& sys, QDataStream& qds){

	int pid; USHORT id; Particle* pp;
	qds >> pid; pp = ps->getParticle(pid);
	qds >> id;
	float x, y, z, r;
	qds >> x >> y >> z >> r;
	Data dt(x, y, z, r);
	pc = new Circle(*pp, sys, id, dt);
	    	
}

	
void Circle::calculate() {

    cs.t[0] = (p.x[Particle::POSN]) - d.x;
    cs.t[1] = (p.x[Particle::POSN+1]) - d.y;
    cs.t[2] = (p.x[Particle::POSN+2]) - d.z;
    cs.t[4] = (cs.t[0])*(cs.t[0]) + (cs.t[1])*(cs.t[1])
	 + (cs.t[2])*(cs.t[2]);

    cs.C->setElem((cs.t[4] - d.r),idnum);
	

    cs.t[5] = 2.0f*( (cs.t[0])*(p.x[Particle::VEL]) 
	+ (cs.t[1])*(p.x[Particle::VEL+1]) 
	+ (cs.t[2])*(p.x[Particle::VEL+2]));
    cs.Cdot->setElem(cs.t[5], idnum);

    
    for(USHORT i= 0; i < 3 ;i++) {
    	cs.J->setElem(2.0*(cs.t[i]), idnum, (p.id()*3)+i);	
    }

    for(USHORT i= 0; i < 3 ;i++) {
    	cs.Jdot->setElem(2.0*(p.x[Particle::VEL+i]), 
	idnum, (p.id()*3)+i);	
    }
}


const Circle::Data & Circle::getData()const {
  return d;
}


bool Circle::usesParticle(const Particle* pc)const {
	if(&p == pc) return true;
	else return false;
}


///////////////////////////////
// Dimension Methods

Dimension::Dimension(Particle& pl, ConstraintSystem& sys,
		unsigned int id, int dimension):
    Constraint(sys, id), p(pl), dim(dimension) {

    posn = pl.x[Particle::POSN+dim];
}


Dimension::Dimension(const Dimension & h):
    Constraint(h.cs, h.idnum), posn(h.posn), p(h.p),
    dim(h.dim){}


void Dimension::calculate() {
    float c = p.x[Particle::POSN+dim] - posn;
    cs.C->setElem(c,idnum);

    c = p.x[Particle::VEL+dim];
    cs.Cdot->setElem(c, idnum);

    cs.J->setElem(1.0f, idnum, (p.id()*3)+dim);

}


bool Dimension::usesParticle(const Particle* pc)const {
	if(&p == pc) return true;
	else return false;
}


QDataStream& Dimension::operator<<(QDataStream& qds) const {
	qds << ConstraintSystem::DIMENSION;
	qds << p.id() << idnum << dim;
	return qds;	
}


void Dimension::buildFromFile(ParticleSystem* ps, Constraint*& pc,
	ConstraintSystem& sys, QDataStream& qds){

	int p; Particle* pp;
	qds >> p; pp = ps->getParticle(p);	
	USHORT id, d;
	 qds >> id >> d;
	
	 pc = new Dimension(*pp, sys, id, d); 	
}


///////////////////////////////
//  ConstraintSystem Methods

ConstraintSystem::ConstraintSystem(ParticleSystem & p):
	ps(p) {
	constraints = new std::vector<Constraint*>();
	t = new float[9];
	C = Cdot = 0;J = Jdot = Jt = LHS= 0;Q = final = 0;
	M = 0;q = qdot = lamda = b= 0;
	kd = ks = 0.5;	
	changed = true;
}	


ConstraintSystem::~ConstraintSystem(){
	for (USHORT i = 0 ; i < constraints->size(); i++) {
		delete ((*constraints)[i]);
	}
	delete constraints;
	delete C; delete Cdot; delete Q;	
	delete J; delete Jdot; delete Jt;
	delete M; delete q; delete b; delete LHS;
	delete qdot; delete lamda; delete final;
	delete [] t;
	
}



void ConstraintSystem::addDistConstraint(Particle& p1,

	Particle& p2, float length) {
	Constraint* c = new Distance(p1, p2, *this,
		constraints->size(), length);
	constraints->push_back(c);
	changed = true;	
}



void ConstraintSystem::addDimensionConstraint(Particle &p, int dim) {
	Constraint* c = new Dimension(p, *this, constraints->size(), dim);
	constraints->push_back(c);
	changed = true;	
}


void ConstraintSystem::addCircleConstraint
	(Particle& p, Circle::Data& d){
	Constraint* c = new Circle(p, *this, constraints->size(), d);
	constraints->push_back(c);
	changed = true;	
}



void ConstraintSystem::fixParticle(Particle &p){
    for(int i = 0; i<3; i++) addDimensionConstraint(p, i);

}



void ConstraintSystem::clear() {
	for (USHORT i = 0 ; i < constraints->size(); i++) {
		delete ((*constraints)[i]);
	}

	constraints->clear();
	changed = true;
}


bool ConstraintSystem::removeParticleRefs(const Particle *p){
	bool alter = false; bool b = false;
	Iter i = constraints->begin();
    while(i != constraints->end()) {
    	b = (*i)->usesParticle(p);
    	if(b) {
    		delete *i;
    		i = constraints->erase(i);
    		alter = true;
    	}
    	else i++;	
    }

    if(alter) {
    	// renumber the constraints
        Iter i = constraints->begin(); int num = 0;
        while(i != constraints->end())
        	(*(i++))->id(num++);
    }

    if(changed || alter) changed = true;
    return alter;
}


void ConstraintSystem::adjustForces() {

	//cout << "entering adjustForces"<< endl;
	
	if(constraints->size() == 0 || ps.size() == 0) return;
	bool partres = ps.isChanged();
	if(changed || partres ) initialize(partres);
	
	// now fill q and qdot with the required values
	Particle *p = 0;
	for (USHORT i = 0; i < ps.size(); i++) {
		p = ps[i];
		q->setElem(p->x[Particle::POSN],3*i);
		q->setElem(p->x[Particle::POSN+1],(3*i)+1);
		q->setElem(p->x[Particle::POSN+2],(3*i)+2);
		
		qdot->setElem(p->x[Particle::VEL],3*i);
	    qdot->setElem(p->x[Particle::VEL+1],(3*i)+1);
	    qdot->setElem(p->x[Particle::VEL+2],(3*i)+2);
	
	    Q->setElem(p->x[Particle::FORCE], 3*i);
	    Q->setElem(p->x[Particle::FORCE+1], 3*i+1);
	    Q->setElem(p->x[Particle::FORCE+2], 3*i+2);
	
	}
#ifdef CONSTRAINT_DB	
	PR(*q); PR(*qdot); PR(*Q);
#endif	
	// now get the constraint objects to fill
	// the J and Jdot matrices
	J->zero();
	Jdot->zero();
	
	
	for (USHORT i = 0; i < constraints->size();i++)
		((*constraints)[i])->calculate();
		
	// solve the matrix eqn.
	*C = ks* (*C);
	*Cdot = kd * (*Cdot);	
	*b = ((-1.0f)*((*Jdot)*(*qdot))) - ((*J)*(*Q)) - (*C) - (*Cdot);
#ifdef CONSTRAINT_DB
	PR(*C); PR(*Cdot);
	PR(*b); PR(*J);
#endif
	
	*Jt = *J;
	Jt->transpose();
	*LHS = (*J)*(*Jt);
#ifdef CONSTRAINT_DB	
	PR(*Jt); PR(*LHS);
#endif	
	ChSolve((*LHS), (*lamda), (*b));
	
	// and finally.
	*final = (*Jt)*(*lamda);
#ifdef CONSTRAINT_DB	
	PR(*final); PR(*lamda);
#endif		
	//redistribute to force vectors.
	for(unsigned int i = 0;i < ps.size();i++) {
		(ps[i])->x[Particle::FORCE] += final->getElem(i*3);
		(ps[i])->x[Particle::FORCE+1] += final->getElem(i*3+1);
		(ps[i])->x[Particle::FORCE+2] += final->getElem(i*3+2);
	}
	
	

	changed = false;
}


//  Constructs all Matrices and Vectors to the
//correct dimemsions.

void ConstraintSystem::initialize(bool psChanged) {
#ifdef CONSTRAINT_DB
	std::cout <<"entering initialize" << std::endl;
#endif
	// the size of the vectors q and qdot (and M) only depend
	// on the number of particles, not the number of
	// constraints.
	
	// the size of C, Cdot and lamda only depend on the number
	// of constraints, not the number of particles
	
	// the size of J , Jt and Jdot depend on both the number of
	// particles and the number of constraints.
    USHORT p = (USHORT)( 3 *  ps.size());
	USHORT c  = (USHORT) constraints->size();
	
	if(psChanged) {
		// we need to change the size of q, qdot, M
		delete q; q = new Vectf(p);
		delete qdot; qdot = new Vectf(p);
		delete Q; Q =  new Vectf(p);
		delete final; final = new Vectf(p);
		// M matrix not used at the moment.
	}
	
	if(changed) {
		delete C; C = new Vectf(c);
		delete Cdot; Cdot = new Vectf(c);
		delete lamda; lamda = new Vectf(c);	
		delete b; b = new Vectf(c);	
		delete LHS; LHS = new Matrixf(c, c);
	}
	
	delete J; J = new Matrixf(c, p);
	delete Jdot; Jdot = new Matrixf(c,p);
	delete Jt; Jt = new Matrixf(p, c);
	

}


const Constraint* ConstraintSystem::getConstraint(unsigned int index)const {
	if (index >= constraints->size()) return 0;
	return (*constraints)[index];
}



unsigned int ConstraintSystem::size() const {
	return constraints->size();
}

// uses the same particleSystem as it always had,
// so watch out.
void ConstraintSystem::loadFromFile(QDataStream& qds){
	clear();
	Constraint* pc = 0; bool exit = false;
	ConstraintType in;
	while (!exit) {
		qds >> in;
		switch(in) {
			
			case END :
				exit = true; pc = 0;
				break;
			case DISTANCE :
				Distance::buildFromFile(&ps, pc, *this, qds);
				break;
			case CIRCLE :
				Circle::buildFromFile(&ps, pc, *this, qds);
				break;
			case DIMENSION :
				Dimension::buildFromFile(&ps, pc, *this, qds);
				break;
			default:
				std::cout << "ConstraintSystem::loadFromFile : "<<
					"fallen thru case!!"<< std::endl;	
		}
		
	    if(pc) constraints->push_back(pc);
	    pc = 0;
	}
    changed = true;
}

//--------------------------------------------------------
// Global IO
//--------------------------------------------------------

QDataStream& operator<<(QDataStream& qds,
	const ConstraintSystem::ConstraintType& ct){
	
	qds << (int) ct; return qds;			
}

QDataStream& operator>>(QDataStream& qds,
	ConstraintSystem::ConstraintType& ct){
	
	int i; qds >> i;
	ct = static_cast<ConstraintSystem::ConstraintType>(i);
	return qds;	
}


QDataStream& operator<<(QDataStream& qds, const Constraint& c){
	c.operator<<(qds);
	return qds;
}

QDataStream& operator<<(QDataStream& qds,
	const ConstraintSystem& cs) {

    qds << ParticleSystem::CONSTRAINTSYSTEM ;
    ConstraintSystem::Iter i = cs.constraints->begin();
    for(;i != cs.constraints->end();i++) {
    	qds << **i;	
    }

    qds << ConstraintSystem::END;
	return qds;	
}
