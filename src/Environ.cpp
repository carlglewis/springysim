#include <cstdlib>
#include <qdir.h>
#include "Environ.h"

bool getResourcePath(QString &result, const QString &file)
{
	QDir res_dir;
	const char* dir = getenv(RESOURCE_DIR);
	if(dir) {
		res_dir.setPath(QString(dir));
		if(res_dir.exists())
			goto got_dir;
	}
	res_dir.setPath(QString(DATADIR) + "/resource");
	if ( !res_dir.exists() )
		return false;	

got_dir:
	result = res_dir.path() + "/" + file;
	QFile f(result);
	return f.exists();	
}


void printResourceError() {
	std::cerr << "The environment variable " <<  RESOURCE_DIR
		<< " is not defined" << std::endl;
	std::cerr << "This contains various essential resources"
		"for springy_sim"<< std::endl;
}



