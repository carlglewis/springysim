#include "SimToolBar.h"

SimToolBar::SimToolBar( QMainWindow * parent, const char * name ):
    QToolBar(parent, name) {

    run = new QToolButton(this, "run");
    run->setToggleButton(true);
    connect(run, SIGNAL(toggled(bool)),
    	this, SIGNAL(simModeChanged(bool)));
    	
    QPixmap pm;
    bool success = loadPixmap(pm);
    if(success)
    	run->setPixmap(pm);
	
    a = new QLabel("  delta t ", this, "a");
    dt = new ZeroToOne(this, "dt");
    connect(dt, SIGNAL(valueChangedf(float)), 
		this, SIGNAL(deltaTChanged(float)));
	dt->setValue(6);

	b =  new QLabel("  update t ", this, "b");
    update = new QSpinBox(this, "update");
    connect(update, SIGNAL(valueChanged(int)),
		this, SIGNAL(updateTimeChanged(int)));
	update->setValue(30);
	
	c =  new QLabel("  wall bounce ", this, "c");
    walls = new ZeroToOne(this, "walls");
    connect(walls, SIGNAL(valueChangedf(float)),
    	this, SIGNAL(wallcoeffChanged(float)));
    walls->setValue(70);
    	
    d =  new QLabel("  gravity ", this, "d");
    grav = new QSpinBox(this, "grav");
    connect(grav, SIGNAL(valueChanged(int)),
    	this, SIGNAL(gravityChanged(int)));
    grav->setValue(35);	
    	
    e =  new QLabel("  drag ", this, "e");
    drag =  new ZeroToOne(this, "drag");
    connect(drag, SIGNAL(valueChangedf(float)),
    	this, SIGNAL(dragChanged(float)));
    drag->setValue(8);
    	

}


void SimToolBar::setButtonOn(bool on) {
	run->setOn(on);

}


void SimToolBar::setValues(SimData& sd){
	dt->setValuef(sd.dt);
	update->setValue(sd.du);
	walls->setValuef(fabs(sd.wallrest));
	grav->setValue(sd.gravity);
	drag->setValuef(sd.drag);
}


bool SimToolBar::loadPixmap(QPixmap& pix) {
	QString pixFile;
	bool loadSuccess = false;
	if( !getResourcePath(pixFile, GO_PIXMAP) ) {
		goto load_error;
	}

	loadSuccess = pix.load(pixFile);
	if( !loadSuccess ) 
		goto load_error;
	return true;

load_error:
	std::cerr << "Could not load " << pixFile << std::endl;
	return false;
}

