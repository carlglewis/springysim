

#ifndef cl_SimToolBar
#define cl_SimToolBar

#include <qtoolbar.h>
#include <qlabel.h>
#include <qtoolbutton.h>
#include <qpixmap.h>
#include "CustomSpinBox.h"
#include <math.h>
#include <qfile.h>
#include "SimData.h"
#include "Environ.h"

#define GO_PIXMAP "go.png"

class SimToolBar : public QToolBar {
  Q_OBJECT
public:
  SimToolBar( QMainWindow * parent = 0, const char * name = 0 );
  void setValues(SimData&);

public slots:
	void setButtonOn(bool on);

signals:
  void deltaTChanged(float val);
  void updateTimeChanged(int val);
  void wallcoeffChanged(float val);
  void gravityChanged(int val);
  void simModeChanged(bool started);
  void dragChanged(float val);



private:
  ZeroToOne* dt;
  QSpinBox* update;
  ZeroToOne* walls;
  QSpinBox* grav;
  ZeroToOne* drag;
  QLabel *a, *b, *c, *d, *e;
  QToolButton * run;

  // loads pixmap from resources directory
  // returns true on success
  bool loadPixmap(QPixmap& pix);

};

#endif
