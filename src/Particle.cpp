#include <vector>
#include "Particle.h"
#include <iostream>
#include <math.h>


using namespace std;

//-----------------------------------------------------
//  Particle methods
//-----------------------------------------------------

Particle::Particle(int id) {
	idnum = id;
	x = new float[ARRAY_SIZE]; // save a few calls to new.
	mass = 1.0;
	for(int i = 0; i < 12; i++) x[i] = 0.0;
	//default colour is white.
	for(int i = 12; i < 15; i++) x[i] = 1.0;
	fixed = false;
}


Particle::Particle(Particle& p) {
  x = new float[ARRAY_SIZE];
  this->operator=(p);
}

Particle& Particle::operator=(Particle& p) {
	if (this == &p) return *this;
	for (unsigned int i = 0; i < ARRAY_SIZE; i++) x[i] = p.x[i];
	mass = p.mass;
	fixed = p.fixed;
	return *this;	
}


Particle::~Particle() {
 	delete[] x;
}

void Particle::setFixed(bool b){
	fixed = b;
}

bool Particle::isFixed() const{
	return fixed;
}

void Particle::pickColor() {
	int i, j;
	float f, rounded;
 	for(int q = 0; q < 3 ; q++) {
		i = rand();
   		j = rand();
   		f = ((float) i) / ((float) j);
   		rounded = floor(f);
   		f = f - rounded;
		//f = abs(f);
		x[COLOR+q] = f + 0.3;
	 }
}


unsigned int Particle::id() const {
	return idnum;
}

void Particle::id(unsigned int i){
	idnum = i;
}


//-----------------------------------------------------
//  Global IO functions
//-----------------------------------------------------


ostream& operator<< (ostream & os, const Particle* p) {
	for (unsigned int i = 0; i < Particle::ARRAY_SIZE; i++ )  {
		cout << p->x[i] << " ";
		if ((i+1)%3 == 0) cout << "| ";
	}
	cout << endl;
	return os;
}


QDataStream& operator<< (QDataStream& qds, const Particle& p){
	for (unsigned int i = 0; i < Particle::ARRAY_SIZE; i++ )  {
		qds << p.x[i];
	}
	qds << p.idnum;
	qds << (int) p.fixed;
	qds << p.mass;
	return qds;
}


QDataStream& operator>> (QDataStream& qds, Particle& p){
    for (unsigned int i = 0; i < Particle::ARRAY_SIZE; i++ )  {
		qds >> p.x[i];
	}
	int i;
	qds >> p.idnum;
	qds >> i;
	p.fixed = static_cast<bool>(i);
	qds >> p.mass;
	return qds;
}


QDataStream& operator<<(QDataStream & qds,
	const ParticleSystem::SysElement& e){
	qds <<(int) e;
	return qds;		
}


QDataStream& operator>>(QDataStream & qds,
	ParticleSystem::SysElement& e){
	int i;
	qds >> i;
	e = static_cast<ParticleSystem::SysElement>(i);
	return qds;
}


QDataStream& operator<<(QDataStream & qds,
	const ParticleSystem& ps ){
	QString name = "ParticleSystem Filev0.1";
	qds << name;
	qds << ps.particles->size();
	//write out the particles
	for (unsigned int i=0; i < ps.particles->size();i++)
		qds << *(ps.getParticle(i));
	
	for (unsigned int j=0; j < ps.forces->size();j++)
		qds << *((*ps.forces)[j]);
	qds << ParticleSystem::END;
	
	if (ps.cs)
		if(ps.cs->size() > 0) qds << *(ps.cs);
	
	return qds;
}	


//-----------------------------------------------------
//  ParticleSystem Methods
//-----------------------------------------------------
 	
ParticleSystem::ParticleSystem(int numParticles) {
	particles = new vector<Particle*>;
 	forces = new vector<Force*>;
 	colliders = new vector<D2_Collider*>;
 	for (int i = 0; i < numParticles; i++) {
 		particles->push_back(new Particle(i));
 	}
 	changed = true;
 	cs= 0;
}

ParticleSystem::~ParticleSystem() {
	//delete particles;
	for (unsigned int i = 0; i < particles->size(); i++)
		delete (*particles)[i];
	delete particles;
	
    //delete all the forces
	for (unsigned int i = 0; i < forces->size(); i++)
		delete (*forces)[i];
	delete forces;
	
	//delete Colliders
	for (unsigned int i = 0; i < colliders->size(); i++)
		delete (*colliders)[i];
	delete colliders; 	
		
	delete cs;
}

unsigned int ParticleSystem::size() {
 	return particles->size();
}

const vector<Particle*>* ParticleSystem::getSystem() const {
 	return particles;
}


void ParticleSystem::addForce(Force* f) {
 	forces->push_back(f);
}


void ParticleSystem::addCollider(D2_Collider* c) {
 	colliders->push_back(c);
}



Particle* ParticleSystem::addParticle(float x, float y, float z) {
	
	Particle *p = new Particle(particles->size());
	p->x[Particle::POSN] = p->x[Particle::LASTX] = x;
	p->x[Particle::POSN+1] = p->x[Particle::LASTX+1] = y;
	p->x[Particle::POSN+2] = p->x[Particle::LASTX+2] = z;
	particles->push_back(p);
	changed = true;
    return p;

}


Particle* ParticleSystem::getParticle(int index) const {
 	return (*particles)[index];
}


Particle* ParticleSystem::operator[](int index) const {
 	return (*particles)[index];
}

void ParticleSystem::clear() {
    Particle*  p;
    while(!particles->empty()) {
		p = particles->back();
		particles->pop_back();
		delete p;
		p = 0;
    }
    clearSprings();
    changed = true;
}

void ParticleSystem::clearSprings() {
	Spring* s = 0;
	vector<Force*>::iterator i = forces->begin();
    while(i != forces->end()) {
		s = dynamic_cast<Spring*>(*i);
		if(s) {  i = forces->erase(i); delete s; }
		else i++;
		s = 0;
    }
}



int ParticleSystem::numForces() const{
	return forces->size();
}

const Force* ParticleSystem::getForce(int index) const {
	return (*forces)[index];
}

void ParticleSystem::calculateForces() {
	
	//first zero forces
	Particle* p;
	for (unsigned int i = 0; i < particles->size(); i++) {
		p = (*particles)[i];
		p->x[Particle::FORCE] = 0;
		p->x[Particle::FORCE+1] = 0;
		p->x[Particle::FORCE+2] = 0;
	}
		
	for(unsigned int i = 0; i < forces->size(); i++)
		(*forces)[i]->applyForce();
		
	if(cs) cs->adjustForces();
}


void ParticleSystem::doCollisions() {
	if (cs) return;
	for (unsigned int i = 0; i < colliders->size();i++)
		(*colliders)[i]->solveCollisions();

}



void ParticleSystem::renumber() {
	for(unsigned int i = 0; i < particles->size();i++)
		((*particles)[i])->id(i);
}


ConstraintSystem* ParticleSystem::enableConstraints(bool b) {
	if(b) {
		delete cs;
		cs = new ConstraintSystem(*this);
		return cs;
	}
	delete cs; cs = 0; return 0;

}


ConstraintSystem* ParticleSystem::getCS() {
	return cs;
}


void ParticleSystem::removeParticle(Particle* p) {
	
	//make sure the particle exists
	P_Iter j = particles->begin();
	bool found = false;
 	while ( j != particles->end() ) {
		if(*j == p) {
			found = true;
		    break;
		}
		else j++;		
	}
    if(!found) {   //bum pointer passed in
    	cout << "Bad pointer passed to "<<
    		"ParticleSystem::removeParticle()"<< endl;
    	return;
    }
	
    // get rid of any Constraints that refer to
    // the particle
	if(cs) cs->removeParticleRefs(p);
	
	// get rid of forces likewise
	F_Iter i = forces->begin();
	while( i != forces->end() ) {
		if( (*i)->usesParticle(p) ){
			delete *i;
			i = forces->erase(i);	
		}
		else i++;
	}
	
	//now actually remove the particle
	delete *j;
	particles->erase(j);
	
	//renumber particles
	j = particles->begin(); int k = 0;
	while ( j != particles->end() ) {	
		(*(j++))->id(k++);		
	}
	changed = true;	
}


bool ParticleSystem::isChanged() {
	bool ret = changed;
	changed = false;
	return ret;
}

// used in loading from file
void ParticleSystem::clearAllForces() {
	F_Iter i = forces->begin();
    while(i != forces->end()) {
		delete *i;
		i = forces->erase(i);
    }	
}

void ParticleSystem::load(QDataStream& qds,
	Global_Drag*& d, Const_Field*& f) {
	clear(); clearAllForces();
	QString	s;
	qds >> s;
	if(s != "ParticleSystem Filev0.1"){
		cout << "ParticleSystem incorrect file format!!"<< endl;
		//*d = 0; *f=0;
		cout << s << endl;
		return;
	}
	
	// make particles.
	int nump;
	qds >> nump;
	Particle*p;
	for(int i=0;i < nump;i++){
		p = new Particle();
		qds >> *p;
		particles->push_back(p);
	}	
	
	// make forces
	SysElement e; bool exit = false;
	Force* pf = 0;	
	while(!exit) {
		qds >> e;
		
		switch(e) {
			case END :
				exit = true; pf = 0;
				break;
			case  CONST_FIELD :
				  f = new Const_Field(this,qds);
				  pf = f;
				  break;
			case SPRING :
				  pf = new Spring(this, qds);
				  break;
			case GLOBAL_DRAG :
				  d = new Global_Drag(this,qds);
				  pf = d;
				  break;
			case DRAG :
				  pf = new Drag(this,qds);
				  break;
			case CONSTANT:
				  pf = new Constant(this, qds);
				  break;
			default:
				  cout<< "ParticleSystem::load()"<<
				  	"fallen thru case!!"<<endl;
				
		
		}
	    if(pf) forces->push_back(pf);
	}
	
	//make constraints (if any)
	if(qds.atEnd()) {
		enableConstraints(false);
		return;
	}
	
	//make the constraints
	qds >> e;
	if(e != CONSTRAINTSYSTEM)
		cout << "ParticleSystem::load() : file format error2"<<
			endl;
			
	if(!cs)
		enableConstraints(true);
	cs->loadFromFile(qds);
	changed = true;
}

