#ifndef cl_io_matrix
#define cl_io_matrix
#include <iostream>
#include "Matrix.h"

template< class T>
std::ostream& operator<<(std::ostream & os, Matrix::Base<T> & b);

template< class T>
std::ostream& operator<<(std::ostream & os, Matrix::Vector<T> & v);

#include "matrix_io.cpp"
#endif


