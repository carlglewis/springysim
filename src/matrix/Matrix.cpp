





template<class T>
bool Matrix::Base<T>::operator==(Base& m) const {
	if(! sameShape(m)) return false;
	for (USHORT i = 0; i < numrows(); i++) {
		for (USHORT j = 0; j < numcols(); j++) {
			if (getElem(i, j) != m.getElem(i, j))
			return false;
		}
	}
	return true;
}


template<class T>
bool Matrix::Base<T>::sameShape(const Base & m) const {
	if(numrows() != m.numrows()) return false;
	if(numcols() != m.numcols()) return false;
	return true;
}


////////////////////////////////////////////
// Matrix::LMatrix<T>::Inner<T> Member functions
////////////////////////////////////////////


// basic constructor
template<class T>
Matrix::LMatrix<T>::Inner::Inner(USHORT i, USHORT j) {
	rows = i; cols = j;
	numrefs = 1;
	data = (T*) new char[sizeof(T) * i * j];
}

// copy constructor - makes a deep copy of the
// Inner
template<class T>
Matrix::LMatrix<T>::Inner::Inner(Inner & inner) {
	numrefs = 1;
	rows = inner.rows; cols = inner.cols;
	USHORT max = rows * cols;
	data = (T*) new char[sizeof(T) * max];
	T* p = data; T* q = inner.data;
	for (; p < (data + max); p++,q++) *p = *q; 	
}


//destructor
template<class T>
Matrix::LMatrix<T>::Inner::~Inner() {
	delete [] data;
}


template<class T>
inline void Matrix::LMatrix<T>::Inner::attach() {
	numrefs++;
}


template<class T>
void Matrix::LMatrix<T>::Inner::detach() {
	if (--numrefs == 0) delete this;
}


template<class T>
inline USHORT Matrix::LMatrix<T>::Inner::numrows() {
	return rows;
}


template<class T>
inline USHORT Matrix::LMatrix<T>::Inner::numcols() {
	return cols;
}

template<class T>
typename Matrix::LMatrix<T>::Inner*
Matrix::LMatrix<T>::Inner::unalias() {
	if(numrefs == 1) return this;
	return new Inner(*this);
}

///////////////////////////////////////////
//  LMatrix Member functions
///////////////////////////////////////////

// standard constructor
template<class T>
Matrix::LMatrix<T>::LMatrix(USHORT numrows, USHORT numcols){
	n = new Inner(numrows, numcols);
}

// copy constructor
template<class T>
Matrix::LMatrix<T>::LMatrix(const LMatrix & m) {
		n = m.n;
		n->attach();
}

//assignment operators
template<class T>
Matrix::LMatrix<T> &
Matrix::LMatrix<T>::operator=(const LMatrix & m) {
	if(! (&m == this)) {
		n->detach();
		n = m.n;
		n->attach();
	}
	return *this;
}

/*template<class T>
Matrix::Base<T> &
Matrix::LMatrix<T>::operator=(Base & b) {
	if (!(&b == this)) {
		LMatrix<T>* p = 0; Base<T> * bp = &b;
		p = dynamic_cast<LMatrix<T> *>(bp);
		if(p) this->operator=(*p);
		else {
			USHORT r, c;
			n = new Inner(r = b.numrows(), c = b.numcols());
			for (USHORT i = 0; i < r ; i++)
				for (USHORT j = 0; j < c ; j++)
					n->data[i*c +j] = b.getElem();
		}
	}
	return *this;
} */

//destructor
template<class T>
Matrix::LMatrix<T>::~LMatrix() {
	n->detach();
}

template<class T>
USHORT Matrix::LMatrix<T>::numrows() const {
	return n->numrows();
}


template<class T>
USHORT Matrix::LMatrix<T>::numcols() const {
	return n->numcols();
}


template<class T>
T Matrix::LMatrix<T>::getElem(USHORT i, USHORT j) const
#ifdef MX_BOUNDS_CHK
		throw(IndexError)
#endif
{
#ifdef MX_BOUNDS_CHK
	if( (i>= n->numrows()) || (j>= n->numcols()) )
		throw IndexError();
#endif
		return n->data[i*(n->numcols()) + j];
}


template<class T>
void Matrix::LMatrix<T>::transpose() {
	Inner* old = n;
	USHORT orows, ocols;
	n = new Inner(ocols = n->numcols(),orows = old->numrows());
	for (USHORT i = 0; i < orows; i++)
		for (USHORT j = 0; j < ocols; j++)
			n->data[j*orows+ i] = old->data[i*ocols + j];
			
	old->detach();	
}


template<class T>
void Matrix::LMatrix<T>::setElem(T val, USHORT i, USHORT j)
#ifdef MX_BOUNDS_CHK
		throw(IndexError)
#endif
{
#ifdef MX_BOUNDS_CHK
	if( (i>= n->numrows()) || (j>= n->numcols()) )
		throw IndexError();
#endif
	n = n->unalias();
	n->data[i*(n->numcols())+ j] = val;
}

template<class T>
void Matrix::LMatrix<T>::zero() {
	n = n->unalias();
	USHORT max = n->numrows() * (n->numcols());
	T* p = n->data;
	for(; p < (n->data + max);p++) *p = 0;
}

// scalar multiplication
template<class T>
Matrix::LMatrix<T>
Matrix::LMatrix<T>::operator*(const T &t) const {
	USHORT rows, cols, max;
	LMatrix<T> ret(rows = n->numrows(), cols = n->numcols());
	T* p = n->data; T* q = ret.n->data;
	max = rows*cols;
	for(; p < (n->data + max);p++, q++) (*q) = (*p) * t;
	return ret;
}


///////////////////////////////////
//  Diagonal Member functions
///////////////////////////////////


//constructor
template<class T>
Matrix::Diagonal<T>::Diagonal(USHORT size) :
	_size(size) {
	
	data = new T[_size];
}


// copy constructor
// no lazy copy
template<class T>
Matrix::Diagonal<T>::Diagonal(const Diagonal & d) {
	_size = d._size;
	data = new T[_size];
	for (USHORT i = 0; i < _size; i++)
		data[i] = d.data[i];
}


// assignment operator
template<class T>
Matrix::Diagonal<T> &
Matrix::Diagonal<T>::operator=(const Diagonal& d) {
	if(&d != this) {
		if (_size != d._size ){
			delete [] data;
			_size = d._size;
			data = new T[_size];
		}
		for (USHORT i = 0; i < _size; i++)
			data[i] = d.data[i];
	}
	return *this;
}


// destructor
template<class T>
Matrix::Diagonal<T>::~Diagonal() {
	delete [] data;
}


template<class T>
void Matrix::Diagonal<T>::setElem(T val, USHORT i, USHORT j)
#ifdef MX_BOUNDS_CHK
		throw(IndexError)
#endif
{
#ifdef MX_BOUNDS_CHK
	if(i >= _size || j >= _size || i != j)
		throw IndexError();
#endif
 	data[i] = val;
}

template<class T>
T Matrix::Diagonal<T>::getElem(USHORT i, USHORT j) const
#ifdef MX_BOUNDS_CHK
		throw(IndexError)
#endif
{
#ifdef MX_BOUNDS_CHK
	if(i >= _size || j >= _size)
		throw IndexError();
#endif
	if (i != j) return 0;
	return data[i];
}

template<class T>
void Matrix::Diagonal<T>::zero() {
	for(USHORT i = 0; i < _size; i++) {
		data[i] = 0; 	
	}
}

// scalar multiplication
template<class T>
Matrix::Diagonal<T>
Matrix::Diagonal<T>::operator*(const T& t) const{
	Diagonal<T> d(_size);
	for(USHORT i = 0; i < _size; i++)
		d.data[i] = t * (data[i]);
	return d;
}

template<class T>
void Matrix::Diagonal<T>::identify() {
	for(USHORT i = 0; i < _size; i++)
		data[i] = 1.0;
}





/////////////////////////////////////////
//   Vector Methods
////////////////////////////////////////

template<class T>
Matrix::Vector<T>::Inner::Inner(USHORT i) {
	length = i;
	data = new T[i];
	numrefs = 1;	
}

template<class T>
Matrix::Vector<T>::Inner::Inner(const Inner & n) {
	length = 0;
	data = 0;
	numrefs = 1;
	this->operator=(n);
}

template<class T>
typename Matrix::Vector<T>::Inner &
Matrix::Vector<T>::Inner::operator=(const Inner & n ) {
	if(this != & n) {
		if(length != n.length) {
			length = n.length;
			delete [] data;
			data = new T[length];
		}
		for (USHORT i = 0; i < length; i++)
			data[i] = n.data[i];	
	}
	return *this;
}	

template<class T>
Matrix::Vector<T>::Inner::~Inner() {
	delete [] data;
}


template<class T>
inline void
Matrix::Vector<T>::Inner::attach() {
	++numrefs;	
}


template<class T>
void
Matrix::Vector<T>::Inner::detach() {
	if(--numrefs == 0) delete this;
}


template<class T>
typename Matrix::Vector<T>::Inner*
Matrix::Vector<T>::Inner::unalias() {
	if (numrefs == 1) return this;
	return new Inner(*this);
}


template<class T>
Matrix::Vector<T>::Vector(USHORT length) {
	n = new Inner(length);	
}


template<class T>
Matrix::Vector<T>::Vector(const Vector & v) {
	n = v.n;
	n->attach();
}


template<class T>
Matrix::Vector<T> &
Matrix::Vector<T>::operator=(const Vector & v) {
	if( this != &v) {
		n->detach();
		n= v.n;
		n->attach();
	}
	return *this; 	
}


template<class T>
Matrix::Vector<T>::~Vector() {
	n->detach();
}


template<class T>
USHORT
Matrix::Vector<T>::length() const {
	return n->length;
}

template<class T>
void
Matrix::Vector<T>::init(T value) {
	n = n->unalias();
	for (int i = 0; i < n->length; i++)
		n->data[i] = value;

}
template<class T>
void
Matrix::Vector<T>::setElem(const T & val, USHORT index)
#ifdef MX_BOUNDS_CHK
		throw(Matrix::IndexError)
#endif
{
#ifdef MX_BOUNDS_CHK
	if(index >= n->length) throw IndexError();
#endif	
    n = n->unalias();
	n->data[index] = val;

}

template<class T>
T
Matrix::Vector<T>::getElem(USHORT index) const
#ifdef MX_BOUNDS_CHK
		throw(Matrix::IndexError)
#endif
{
#ifdef MX_BOUNDS_CHK
	if(index >= n->length) throw IndexError();
#endif	
	return n->data[index];
}

template<class T>
T
Matrix::Vector<T>::dotprod(const Vector & v) const
#ifdef MX_BOUNDS_CHK
		throw(Matrix::SizeError)
#endif
{
#ifdef MX_BOUNDS_CHK
	if(n->length != v.n->length) throw SizeError();
#endif	
	T res = 0;
	for(USHORT i = 0; i < n->length; i++)
		res += (n->data[i] * (v.n->data[i]));
	return res;
}



template<class T>
Matrix::Vector<T>
Matrix::Vector<T>::operator+(const Matrix::Vector<T> & v) const
	throw (Matrix::SizeError) {

	if (n->length != v.n->length) throw Matrix::SizeError();
	Matrix::Vector<T> ret(n->length);
	for (USHORT i = 0; i < n->length; i++) {
		ret.n->data[i] = n->data[i] + v.n->data[i];
	} 	
	return ret;
}



template<class T>
Matrix::Vector<T>
Matrix::Vector<T>::operator-(const Matrix::Vector<T> & v) const
	throw (Matrix::SizeError) {

	if (n->length != v.n->length) throw Matrix::SizeError();
	Matrix::Vector<T> ret(n->length);
	for (USHORT i = 0; i < n->length; i++) {
		ret.n->data[i] = n->data[i] - v.n->data[i];
	} 	
	return ret;
}


/////////////////////////////////////////
// Global Functions
/////////////////////////////////////////

/*ostream& operator<< (ostream & os, Matrix::Error & e) {
	string s = e.message();
	os << s;
	return os;
} */


/////////////////////////////////////
//  Matrix Addition

template<class T>
Matrix::LMatrix<T>
operator+(const Matrix::Base<T> & bl,
	const Matrix::Base<T> & br)
	throw(Matrix::SizeError) {

	if(!bl.sameShape(br)) throw Matrix::SizeError();
	USHORT rows, cols;
	Matrix::LMatrix<T> ret(rows = bl.numrows(), cols = bl.numcols());
	for (USHORT i = 0; i < rows; i++)
		for (USHORT j = 0; j < cols; j++)
			ret.n->data[i*cols+j] =
				bl.getElem(i, j) + br.getElem(i,j);
	return ret;
}


// takes two LMatrix's instad.
// may be slightly faster
template<class T>
Matrix::LMatrix<T>
operator+(const Matrix::LMatrix<T> & bl,
	const Matrix::LMatrix<T> & br)
	throw(Matrix::SizeError) {

	if(!bl.sameShape(br)) throw Matrix::SizeError();
	USHORT rows, cols, max;
	Matrix::LMatrix<T> ret(rows = bl.numrows(), cols = bl.numcols());
	max = rows*cols;
	for (USHORT i = 0; i < max; i++)
		ret.n->data[i] =
			bl.n->data[i] + br.n->data[i];
	return ret;
}


/////////////////////////////////////
// Matrix multiplication

template<class T>
Matrix::LMatrix<T>
operator*(const Matrix::Base<T> & bl, const Matrix::Base<T> &br)
	throw(Matrix::SizeError) {
	
	USHORT brows;
	if(bl.numcols() != (brows = br.numrows())) throw Matrix::SizeError();
	USHORT newrows, newcols, s;
	T sum;
	s = bl.numcols();
	Matrix::LMatrix<T> ret(newrows = bl.numrows(), newcols = br.numcols());
	for (USHORT i = 0; i < newrows; i++)
		for (USHORT j = 0; j < newcols; j++) {
			sum = 0;
			for (USHORT k = 0; k < s; k++) {
				sum += bl.getElem(i, k) * br.getElem(k, j);
			}
			ret.n->data[i*newrows+j] = sum;
        }
   return ret;
}

template<class T>
Matrix::LMatrix<T>
operator*(const Matrix::LMatrix<T> & ml, const Matrix::LMatrix<T> &mr)
	throw(Matrix::SizeError) {
	
	USHORT ml_rows = ml.n->numrows();
	USHORT ml_cols = ml.n->numcols();
	USHORT mr_rows = mr.n->numrows();
	USHORT mr_cols = mr.n->numcols();
	if(ml_cols != mr_rows) throw Matrix::SizeError();
	T sum;	
	Matrix::LMatrix<T> ret(ml_rows,mr_cols);
	
	for (USHORT i = 0; i < ml_rows; i++)
		for (USHORT j = 0; j < mr_cols; j++) {
			sum = 0;
			for (USHORT k = 0; k < ml_cols; k++) {
				sum += ml.n->data[i*ml_cols + k] *
						mr.n->data[k*mr_cols + j];
			}
			ret.n->data[i*ml_rows+j] = sum;
        }
   return ret;
}


// multiply Diagonal (on left)  with LMatrix	
template<class T>		
Matrix::LMatrix<T> operator*
	(const Matrix::Diagonal<T> & dl, const Matrix::LMatrix<T> & mr)
	throw(Matrix::SizeError) {
	
	//cout << "diag mult" << endl;
	USHORT dl_size = dl._size;
	USHORT mr_rows = mr.n->numrows();
	USHORT mr_cols = mr.n->numcols();
	USHORT index = 0;
	if(dl_size != mr_rows) throw Matrix::SizeError();
	
	Matrix::LMatrix<T> ret(dl_size,mr_cols);
	
	for (USHORT i = 0; i < dl_size; i++) {
		for (USHORT j = 0; j < mr_cols; j++) {
			ret.n->data[index+j] = dl.data[i] * mr.n->data[index+j]; 	
		}
		index += mr_cols;
	}
	 return ret;	
}	
	
// multiply Diagonal (on right) with LMatrix	
template<class T>		
Matrix::LMatrix<T> operator*
	(const Matrix::LMatrix<T> & ml, const Matrix::Diagonal<T> & dr)
	throw(Matrix::SizeError){

    //cout << "diag mult" << endl;
	USHORT ml_rows = ml.n->numrows();
	USHORT ml_cols = ml.n->numcols();
	USHORT dr_size = dr._size;
	USHORT index = 0;
	T mult;
	
	if(ml_cols != dr_size) throw Matrix::SizeError();
	
	Matrix::LMatrix<T> ret(ml_rows, dr_size);
	for (USHORT i = 0; i < ml_cols; i++) {
		mult = dr.data[i]; index = 0;
		for (USHORT j = 0; j < ml_rows; j++) {
			ret.n->data[index+i] = mult * ml.n->data[index+i];
			index += ml_cols;
		}	
	}
	return ret;		
}		

template<class T>
Matrix::Vector<T>
operator*(const T & t, const Matrix::Vector<T> & v) {
	Matrix::Vector<T> res(v.n->length);
	for (int i = 0; i < v.n->length; i++)
		res.n->data[i] = t* (v.n->data[i]);
	return res;			
}

template<class T>
Matrix::Vector<T>
operator*(const Matrix::Vector<T> & v, const T & t) {
	Matrix::Vector<T> res(v.n->length);
	for (int i = 0; i < v.n->length; i++)
		res.n->data[i] = t* (v.n->data[i]);
	return res;		
}

template<class T>		
Matrix::Vector<T> operator*
	(const Matrix::Vector<T> & vl, const Matrix::LMatrix<T> & mr)
	throw(Matrix::SizeError){
	
	USHORT mr_rows, mr_cols;
	if(vl.length() != (mr_rows = mr.numrows()))
		throw Matrix::SizeError("operator*: Vector x LMatrix");
	
	T sum;		
	Matrix::Vector<T> ret(mr_cols = mr.numcols());
	for (USHORT i = 0; i < mr_cols;i++) {
		sum = 0;
		for (USHORT j = 0; j < mr_rows; j++) {
			sum += (vl.n->data[j]) * (mr.n->data[j*mr_cols+i]);
		}
		ret.n->data[i] = sum; 	
	}
	return ret;		
}
	
	
template<class T>		
Matrix::Vector<T> operator*
	(const Matrix::LMatrix<T> & ml, const Matrix::Vector<T> & vr)
	throw(Matrix::SizeError){

	USHORT ml_rows, ml_cols;
	if(vr.length() != (ml_cols = ml.numcols()))
		throw Matrix::SizeError("operator*: LMatrix x Vector");	
	
	T sum;		
	Matrix::Vector<T> ret(ml_rows = ml.numrows());
	
	for (USHORT i = 0; i < ml_rows;i++) {
		sum = 0;
		for (USHORT j = 0; j < ml_cols; j++) {
			sum += (vr.n->data[j]) * (ml.n->data[i*ml_cols+j]);
		}
		ret.n->data[i] = sum; 	
	}
	return ret;		
}



template<class T>
void	ChSolve(Matrix::LMatrix<T> & a, Matrix::Vector<T> & x,
	const Matrix::Vector<T> &b) throw(Matrix::SizeError,
	Matrix::SingularMatrix)
{
	static T p[256];
	T sum;
	USHORT	n = a.numrows();
	USHORT k;
	if(n > 255)
		throw Matrix::SizeError("ChSolve: Matrix too large");

	if(n != a.numcols())
		throw Matrix::SizeError("ChSolve: non-square Matrix");

	if(n != b.length() || n != x.length())
		throw Matrix::SizeError("ChSolve: bad vector length");

	for(USHORT i = 0; i < n; i++)
		for(USHORT j = i; j < n; j++) {
			for(sum = a.getElem(i,j), k = i - 1; k < 255; k--)
				sum -= a.getElem(i,k) * a.getElem(j,k);
			if(i == j) {
				if(sum <= 0) throw Matrix::SingularMatrix();
				p[i] = sqrt(sum);
			}
			else a.setElem((sum / p[i]),j,i);
		}

	for(USHORT i = 0; i < n; i++)
	{
		for(sum = b.getElem(i), k = i - 1; k < 255; k--)
			sum -= a.getElem(i,k) * x.getElem(k);
		x.setElem((sum / p[i]),i);
	}

	for(USHORT i = n - 1; i < 257; i--)
	{
		for(sum = x.getElem(i), k = i + 1; k < n; k++)
			sum -= a.getElem(k,i) * x.getElem(k);
		x.setElem( (sum/p[i]) ,i);
	}
}



