


template<class T>
std::ostream& operator<<(std::ostream& os, Matrix::Base<T> & b) {
	for (USHORT i = 0; i < b.numrows(); i++) {
		for (USHORT j = 0; j < b.numcols(); j++) {
			os.width(os.precision() + 3);
			os << b.getElem(i, j);
		}
		os << endl;
	}
	return os;		
}

template<class T>
std::ostream& operator<<(std::ostream & os, Matrix::Vector<T> & v) {
	for (USHORT i = 0; i < v.length(); i++) {
		os.width(os.precision() + 3);
		os << v.getElem(i);	
	}
	return os;
}

