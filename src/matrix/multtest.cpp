//mtest

#include <iostream>
#include "Matrix.h"
#include "matrix_io.h"

int main() {
	Matrix::LMatrix<float> a(1, 4);
	a.zero();
	a.setElem(1.0f, 0, 2);
    cout <<"a:\n" << a << endl;

    Matrix::LMatrix<float> b(4, 1);
	b.zero();
	b.setElem(2.0f, 2, 0);
    cout <<"b:\n" << b << endl;

     Matrix::LMatrix<float> c(1, 1);

     c = a*b;
     cout <<"c:\n" << c << endl;
}