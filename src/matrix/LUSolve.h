
/*
 *	LUSolve: Functions for solving matrix equations with LU factorisation
 *	Formulae taken from Erwin Kreysig, Advanced Engineering Mathematics, 
 *	7th Ed., p981
 */

#ifndef SPRINGY_SIM_LUSOLVE_H
#define  SPRINGY_SIM_LUSOLVE_H

template<class T>
void genDoolittleLU(const Matrix::LMatrix<T> &A, 
	Matrix::LMatrix<T> &U,
	Matrix::LMatrix<T> &L);


template<class T>
void genCroutLU(const Matrix::LMatrix<T> &A, 
	Matrix::LMatrix<T> &U,
	Matrix::LMatrix<T> &L);


template<class T>
void solveForY(Matrix::LMatrix<T> &L, 
	Matrix::Vector<T> &b, 
	Matrix::Vector<T> &y);


template<class T>
void solveForX(Matrix::LMatrix<T> &U, 
	Matrix::Vector<T> &x, 
	Matrix::Vector<T> &y);


template<class T>
void LUDoolittleSolve (Matrix::LMatrix<T> & a, Matrix::Vector<T> & x,
	const Matrix::Vector<T> & b) throw(Matrix::SizeError,
	Matrix::SingularMatrix);


template<class T>
void LUDoolittleSolve (Matrix::LMatrix<T> & a, Matrix::Vector<T> & x,
	const Matrix::Vector<T> & b) throw(Matrix::SizeError,
	Matrix::SingularMatrix);


#include "LUSolve.cpp"

#endif //  SPRINGY_SIM_LUSOLVE_H




