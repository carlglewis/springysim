#include "Matrix.h"

# define PR(x) (cout << #x << ":\n "<< (x) << endl)
int main() {

	Matrix::Vector<float> a(4);
    Matrix::Vector<float> b(4);

	a.init(1.0);
    b.init(2.0);

    PR(a);
    PR(b);
    cout <<"c = a + b" << endl;
    Matrix::Vector<float> c = a + b;
     PR(c);

    c = c - (2.0f*b);
    cout <<"c = c - (2.0f*b)" << endl;
    PR(c);
}