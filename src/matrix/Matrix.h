

#ifndef cl_matrix
#define cl_matrix
#include <iostream>
#include <string>
#include <math.h>

#define MX_BOUNDS_CHK

typedef unsigned short int USHORT;

namespace Matrix {	
	template<class T>
	class LMatrix;
	template<class T>
	class Diagonal;
	
	template<class T>
	class Vector;
	
	class Error;
	class SizeError;

	//exceptions
    class Error{
    protected:
        std::string* m;
    public:
    	Error(){m= 0;}
    	Error(char* s){ m = new std::string(s); }
    	virtual ~Error(){ delete m; }
    	virtual const std::string message() const {
    		if(m != 0) return  *m;
    		else return "no info";
    	}
    };

    class SizeError : public Error {
    public:
    	SizeError(): Error() {
    		name = new std::string("Matrix::SizeError");
    	}
    	SizeError(char* s):
    	Error(s) {}
    	const std::string message() const {
    		if(m != 0) return  *m;
    		else return *name;
    	}

    private:
	std::string* name;
    };

	class IndexError : public Error {
	public:
		IndexError() : Error() {
		  name =  new std::string("Matrix::IndexError");
		}
		IndexError(char*s):
		Error(s){}
		const std::string message() const {
    		if(m != 0) return  *m;
    		else return *name;
    	}

	private:
		std::string* name;
	};

	class BlockError : public Error{};
	class ReadOnlyError : public Error{};
	class NotYetImplemented : public Error{};
	class SingularMatrix : public Error{
	public:
	    SingularMatrix(): Error() {
	    	name =  new std::string("Matrix::SingularMatrix");
	    }
    	SingularMatrix(char* s):
    	Error(s) {}
    	const std::string message() const {
    		if(m != 0) return  *m;
    		else return *name;
    	}
 	private:
 		std::string* name;
	};
	
} // namespace Matrix

/////////////////////////////////////
// Global friends -
// forward declarations required

// add two LMatrix's
template<class T>		
Matrix::LMatrix<T> operator+
	(const Matrix::LMatrix<T> & lm, const Matrix::LMatrix<T> & rm )
	throw(Matrix::SizeError);

// multiply two LMatrix's	
template<class T>		
Matrix::LMatrix<T> operator*
	(const Matrix::LMatrix<T> & bl, const Matrix::LMatrix<T> & br)
	throw(Matrix::SizeError);	
	
// multiply Diagonal (on left)  with LMatrix	
template<class T>		
Matrix::LMatrix<T> operator*
	(const Matrix::Diagonal<T> & dl, const Matrix::LMatrix<T> & mr)
	throw(Matrix::SizeError);	
	
// multiply Diagonal (on right) with LMatrix	
template<class T>
Matrix::LMatrix<T> operator*
	(const Matrix::LMatrix<T> & ml, const Matrix::Diagonal<T> & dr)
	throw(Matrix::SizeError);	
	
		
	
	

/////////////////////////////////////
//  Classes

namespace Matrix {


//abstract base class for all matrices.
template<class T>
class Base {
public:
	Base(){}
	Base(Base & m){}
	//virtual Base & operator=(Base & rhs)= 0 ;
	virtual ~Base(){}
	virtual bool operator==(Base & m) const;
	virtual void zero() = 0;
	
	//class M_proxy{};
	//M_proxy<T> operator[](int i);
	
	virtual void transpose() = 0;
	
	virtual USHORT numrows() const = 0;
	virtual USHORT numcols() const = 0;
	virtual bool sameShape(const Base & m) const;
	
	virtual T getElem(USHORT i, USHORT j) const
#ifdef MX_BOUNDS_CHK
		throw(IndexError)
#endif
		 = 0;	
};

template<class T>
class Writable : public Base<T> {
public:
	/*virtual Base & operator+=(Base & m) = 0;
	virtual Base & operator*=(Base & m) = 0;
	virtual Base & operator*=(T& t) = 0; */
	virtual void setElem(T val, USHORT i, USHORT j)
#ifdef MX_BOUNDS_CHK
		throw(IndexError)
#endif
 		= 0;
};


//  Literal matrix: exactly the same interface
// as Matrix.  Stores values as an array.
template<class T>
class LMatrix : public Writable<T> {
public:
   	LMatrix(USHORT numrows, USHORT numcols);
	LMatrix(const LMatrix & m);
	LMatrix & operator=(const LMatrix & b);
	//virtual Base & operator=(Base & b);
	virtual ~LMatrix();
	
	virtual USHORT numrows() const;
	virtual USHORT numcols() const;
	virtual void transpose();
		
	LMatrix operator*(const T & t) const;
	
	void setElem(T val, USHORT i, USHORT j)
#ifdef MX_BOUNDS_CHK
		throw(IndexError);
#endif

	T getElem(USHORT i, USHORT j) const
#ifdef MX_BOUNDS_CHK
		throw(IndexError);
#endif
 	
	void zero();
	
/*	// friends
	friend LMatrix operator+<T>(const LMatrix<T> & bl,
	const LMatrix<T> & br)
	throw(SizeError);
	
	friend LMatrix operator*<T>(const LMatrix<T> & bl,
	const LMatrix<T> & br)
	throw(SizeError);

	// multiply Diagonal (on left)  with LMatrix	
	friend LMatrix<T> operator*<T>
	(const Diagonal<T> & dl, const LMatrix<T> & br)
	throw(SizeError);
	
	// multiply Diagonal (on right) with LMatrix	
	friend LMatrix<T> operator*<T>
	(const LMatrix<T> & bl, const Diagonal<T> & dr)
	throw(SizeError);	

	friend Matrix::Vector<T>
	operator*<T> (const Matrix::Vector<T> & vl, const Matrix::LMatrix<T> & mr)
	throw(Matrix::SizeError);

	friend Matrix::Vector<T>
	operator*<T> (const LMatrix<T> & ml, const Vector<T> & vr)
	throw(Matrix::SizeError);     */

	class Inner{
	public:
		Inner(USHORT i, USHORT j);
		Inner(Inner & inner);
		~Inner();
		T* data;
		void attach();
		void detach();
		USHORT numrows();
		USHORT numcols();
		Inner* unalias();	
	private:
		USHORT numrefs;
		USHORT rows;
		USHORT cols;
	};
	
public:
	Inner* n;

};


// Sparse matrix - holds values in linked lists
// All values not explicitly set are zero.
// Same interface as matrix
template<class T>
class SMatrix : public Writable<T> {
	

};


//wraps some other data structure up into
// a matrix or vector.  gets its data from
// some other object of class S.
// only supports read-only operations.
template<class T, class S>
class PMatrix : public Base<T> {

};



//  This class is used for diagonal matrices
// all entries not on the main diagonal are zero
// Forgetting it is strictly diagonal will probably
// mean an exception gets thrown
// diagonal matrices are always square
// no lazy copying at this stage.
template<class T>
class Diagonal : public Base<T> {
public:
	explicit Diagonal(USHORT size);
	Diagonal(const Diagonal& d);
	Diagonal& operator=(const Diagonal & d);
	~Diagonal();
	
	virtual USHORT numrows() const { return _size; }
	virtual USHORT numcols() const { return _size; }
	virtual void transpose(){}
	
	void setElem(T val, USHORT i, USHORT j)
#ifdef MX_BOUNDS_CHK
		throw(IndexError)
#endif
     ;
	T getElem(USHORT i, USHORT j) const
#ifdef MX_BOUNDS_CHK
		throw(IndexError)
#endif
	;
	
	void zero();
	Diagonal operator*(const T & t) const;
	
	// multiply Diagonal (on left)  with LMatrix	
	//template<class T> friend LMatrix<T> operator* < T > (const Diagonal<T> & dl, const LMatrix<T> & mr)
	//    throw(SizeError);	
	
	//multiply Diagonal (on right) with LMatrix	
	//friend LMatrix<T> operator*<T>
	//(const LMatrix<T> & ml, const Diagonal<T> & dr)
	//throw(SizeError);	
	
	void identify();
	
    private:
    	T* data;
    	USHORT _size;
    	
};


template<class T>
class Vector {
public:

	explicit Vector(USHORT length);
	Vector(const Vector & v);
	Vector& operator=(const Vector & v);
	~Vector();
	
	USHORT length() const ;
	void init(T value);
	
	void setElem(const T & val, USHORT index)
#ifdef MX_BOUNDS_CHK
		throw(IndexError);
#endif
    ;	
	T getElem(USHORT index) const
#ifdef MX_BOUNDS_CHK
		throw(IndexError);
#endif
    ;

    T dotprod(const Vector & v) const
#ifdef MX_BOUNDS_CHK
		throw(SizeError);
#endif
    ;

/*    friend Matrix::Vector<T>
	operator*<T>(const T & t, const Matrix::Vector<T> & v);

	friend Matrix::Vector<T>
	operator*<T>(const Matrix::Vector<T> & v, const T & t);
	
	friend Matrix::Vector<T>
	operator*<T> (const Matrix::Vector<T> & vl, const Matrix::LMatrix<T> & mr)
	throw(Matrix::SizeError);

	friend Matrix::Vector<T>
	operator*<T> (const Matrix::LMatrix<T> & ml, const Matrix::Vector<T> & vr)
	throw(Matrix::SizeError);*/
	
	Vector operator+(const Vector& v) const throw(SizeError);
	
	Vector operator-(const Vector& v) const throw(SizeError);
	

//private:
	class Inner {
	public:
		Inner(USHORT i);
		Inner(const Inner & n);
		Inner& operator=(const Inner & n);
		~Inner();
		T* data;
		void attach();
		void detach();
		Inner* unalias();
			
		USHORT numrefs;
		USHORT length;
	};
	
	Inner* n;
	
};


} //namespace Matrix



///////////////////////////////////////
// Global Functions
///////////////////////////////////////
//ostream& operator<< (ostream & os, Matrix::Error & e);

///////////////////////////////////////
// utility routines to make scalar multiplication
// 'commutative'

template<class T>
Matrix::Diagonal<T> operator*(const T& t,
	const Matrix::Diagonal<T> & d) {
	return d.operator*(t);	
}

template<class T>
Matrix::LMatrix<T> operator*(const T & t,
	const Matrix::LMatrix<T> & m) {
	return m.operator*(t);	
}

/////////////////////////////////////////
//  Matrix Addition routines


// add two arbitary matrices
template<class T>
Matrix::LMatrix<T> operator+
	(const Matrix::Base<T> & bl, const Matrix::Base<T> & br)
	throw(Matrix::SizeError);


////////////////////////////////////////	
//  Matrix Multiplication routines		

// multiply any two matrices,
// get an LMatrix back
template<class T>		
Matrix::LMatrix<T> operator*
	(const Matrix::Base<T> & bl, const Matrix::Base<T> & br)
	throw(Matrix::SizeError);
	

///////////////////////////////////////
//	Vector x Scalar routines

template<class T>
Matrix::Vector<T>
operator*(const T & t, const Matrix::Vector<T> & v);

template<class T>
Matrix::Vector<T>
operator*(const Matrix::Vector<T> & v, const T & t);

///////////////////////////////////////
// Vector x LMatrix Routines

template<class T>		
Matrix::Vector<T> operator*
	(const Matrix::Vector<T> & vl, const Matrix::LMatrix<T> & mr)
	throw(Matrix::SizeError);
	
	
template<class T>		
Matrix::Vector<T> operator*
	(const Matrix::LMatrix<T> & ml, const Matrix::Vector<T> & vr)
	throw(Matrix::SizeError);	
	
////////////////////////////////////////
//	Matrix eqn solving routine
//  Code not wholly original
// assumes the matrix is symmetric positive definite
///////////////////////////////////////	

template<class T>
void ChSolve (Matrix::LMatrix<T> & a, Matrix::Vector<T> & x,
	const Matrix::Vector<T> & b) throw(Matrix::SizeError,
	Matrix::SingularMatrix);
	
#include "Matrix.cpp"

////const string Matrix::SizeError::name("Matrix::SizeError");

//const string Matrix::IndexError::name("Matrix::IndexError");

//const string Matrix::SingularMatrix::name("Matrix::SingularMatrix");

#endif



