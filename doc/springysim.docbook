<!DOCTYPE book PUBLIC "-//OASIS//DTD DocBook XML V4.1.2//EN" "http://www.oasis-open.org/docbook/xml/4.1.2/docbookx.dtd" [
<!ENTITY progname "Springysim"> 
]>
 
<book>
<bookinfo>
<title>&progname; User Documentation</title>


<author>
<firstname>Carl</firstname>
<othername>G.</othername>
<surname>Lewis</surname>
<contrib>
Program and Documentation Contributor
</contrib>
</author>


<copyright>
<year> 2001 </year>
<year> 2004 </year>

<holder>
Carl G. Lewis
</holder>
</copyright>

<keywordset>
<keyword>Physics</keyword>
<keyword>Simulation</keyword>
<keyword>springs</keyword>
<keyword>contraints</keyword>
<keyword>linear solver</keyword>

</keywordset>

</bookinfo>
 
<chapter id="introduction">
<title>Introduction</title>
<para>
&progname; is an interactive, 2-dimensional physics simulation.  The elements simulated include point masses, springs, rigid constraints and surfaces.  &progname; is interactive in that it allows you to add arbitrary elements while the simulation is running.
</para>

<para>
&progname; was initially created as a university assignment for a C++ programming course. It's continued to be a learning tool, allowing me to learn about the <ulink url="http://www.trolltech.com/products/qt/index.html">Qt GUI toolkit</ulink>, the <ulink url="http://sources.redhat.com/autobook/">GNU autotools</ulink>, physics simulation techniques and writing documentation with <ulink url="http://www.docbook.org/">Docbook</ulink>. I've made it publicly available mainly for people who are interested in learning about the basics of mechanics simulation. More sophisticated version of the algorithms and methods in &progname; are used to much greater effect in computer games and digital animation.
</para>

<sect1 id="features">
<title>
&progname; Features
</title>
<para>This is a brief list of the basic features of &progname; with links to more detailed information.</para>

<formalpara> <title>2D Particle Simulation</title>
The basic simulation element in &progname; are particles, or point masses. Particles can interact with surfaces (collisions), and interact with each other via springs and constraints. Since point masses have no dimension, they cannot collide with each other.
</formalpara>

<formalpara> <title>2D Spring simulation</title>
Pairs of particles can be joined with springs. Springs conform to Hooke's Law- they exert a force on particles, but have no mass. 
</formalpara>

<formalpara> <title>Adjustable spring parameters</title>
Each spring is described by its Spring Constant and Damping Constant. Different types of springs can be created with different combinations of these parameters. Different spring types can also have different colors in the simulation interface.
</formalpara>

<formalpara> <title>2D Rigid Constraint Simulation</title>
Rigid constraints enforce limits on the absolute and relative positions of particles. Several different types of rigid constraint are supported, including limiting single particles to horizontal or vertical planes and constraining pairs of particles to be a fixed distance from each other. A single particles can have multiple constraints applied to it.
</formalpara>

<formalpara><title>Loading and saving of simulations to file </title>
Simulations including particles, springs constraints and parameters can be saved to file and reloaded.
</formalpara>

<formalpara><title>Particle Tracing</title>
A particle can have its trajectory traced by drawing a line between all the points the particle has occupied at different time steps in the simulation.
</formalpara>

<formalpara><title>Three different Differential Equation Solvers</title>
Different algorithms can be used to solve the equations that describe the forces that act on the particles. The different types of solver provide differing degrees of accuracy and computational complexity. Using the right algorithm is an important part of making a simulation work, and &progname; allows the differences between them to be easily seen.
</formalpara>

<formalpara><title>Adjustable Simulation Parameters</title>
A number of simulation parameters can be adjusted, including the speed of the simulation, the timestep, and the drag force on the particles. These parameters can also be saved in the simulation files.
</formalpara>

</sect1>

<sect1 id="springysim_basics">
<title>
&progname; Basics
</title>

<sect2 id="modes_basics">
<title>
Modes
</title>

<para>
&progname; operates in two different modes, because not all the features it offers can be used at the same time.  The two modes are "Constraints" and "Collisions".  Essentially, if you want the particles (point masses) bounce off the walls, you must be in Collisions mode.  In collisions mode, you can't make any constraints.  And vice versa.  The reasons behind this are explained later.  The two modes have other restrictions too, like the type of Ordinary Differential Equation Solver (ODESolver) that can be used.  In the current version, when you switch between modes, all the elements currently on the screen are removed. Change modes by selecting an option in the Mode menu. See <xref linkend="sim_mode_menu" />.
</para>
</sect2>

<sect2 id="adding_particles">
<title>
Adding Particles
</title>

<para>
Right-click to make a particle wherever you click.  If you want a fixed particle, which is especially handy when there are no walls, just hold down SHIFT while you click.
</para>
</sect2>


<sect2 id="adding_springs">
<title>
Adding Springs
</title>

<para>
To add a spring, you have to make sure that your "MouseMode" is "Make Spring" (this is a different mode not related to <link linkend="modes_basics">these modes</link>).  To do this, just select the "MakeSpring" option from the <link linkend="edit_menu">Edit Menu</link>.  Then it's easy. Just left click a particle, drag to the second particle, and they will be joined by a spring. (This is a easier when the simulation is stopped) </para>
</sect2>

<sect2 id="removing_particles">
<title>
Removing Particles
</title>

<para>
To remove a particle, you need to choose Box Select from the <link linkend="edit_menu">Edit Menu</link>, or press Ctrl-B.  Now, left-click and drag to draw a box around the particle(s) you want to remove. Then press Delete.  The particles will be removed, along with any springs or constraints attached to it.  If you want to get rid of all the particles at once, select from the <link linkend="edit_menu">Edit Menu</link> menu.
</para>
</sect2>

<sect2 id="removing_springs">
<title>
Removing Springs
</title>

<para>
To remove springs individually, you have to use the same method as above - by removing one of the particles which the spring is attached to.  Or, If you want to remove all the springs (but keep the partcles and constraints) select Clear Springs from the <link linkend="edit_menu">Edit Menu</link>.
</para>
</sect2>

</sect1>
</chapter>


<chapter id="springs_in_detail">
<title>Springs in Detail</title>

<sect1 id="spring_toolbar">
<title>
The Spring Toolbar
</title>

<screenshot>
<graphic  fileref="../images/spring_toolbar.png" />
</screenshot>

<para>
The Spring ToolBar allows you to add different kinds of springs to the simulation.  Springs have three parameters:
</para>


<formalpara>
<title>Spring Constant (ks):</title>This is the standard constant in Hook's law which you remember from High School physics.  making this number higher means the spring is stiffer, and will rebound faster.
</formalpara>

<formalpara>
<title>Damping Constant (kd):</title>The springs in &progname; are more realistic than your normal Hooks Law Spring becuase they have damping - they will gradually come to rest and not bounce around forever.  Making kd higher means the spring will come to rest faster.
</formalpara>

<formalpara>
<title>Length:</title>This is the length of the spring in pixels when it is "at rest", i.e. when it is not exerting any force on the particles it is attached to.
</formalpara>

<para>
There are six default spring types in &progname;, each with a different set of values for these parameters. Whenever you add a spring to the simulation, it will be given the parameters that are displayed in the Spring Toolbar. You can alter these values displayed on the Toolbar at anytime by clicking on the up/down arrows.  However, these only affect springs that are yet to be made - once a spring has been placed in the simulation, its parameters cannot be changed.  
</para>

<para>
Choose a different Spring type by pressing on the arrow in combo box on the Spring ToolBar, then selecting one of the types that are listed.  Notice that the parameter values and the color chit change to reflect  the change.
</para>


<sect2 id="spring_types">
<title>
Making your own Spring Types
</title>

<para>
If you want to have more than six different kinds of springs at the
same time, click on the "New Type" Button.  A new type will be
created, with the values currently set. You can edit the name of the
type by clicking in the combo box on the Tool Bar.
</para>
</sect2>

</sect1>

<sect1 id="using_mouse_spring">
<title>Using the Mouse Spring</title>
<para>
The Mouse Spring is an easy and fun way to interact with the simulation. To use the Mouse Spring, press Ctrl while pressing and holding the left mouse button in the simulation area. A line will be drawn between the mouse pointer and the nearest particle. This particle can then be dragged around the simulation with the mouse.  The particle will be released when the left mouse button is released.
</para>
</sect1>


</chapter>


<chapter id="using_constraints">
<title>
Using Constraints
</title>

<sect1 id="constraint_types">
<title> Constraint Types </title>
<para>
Now for the fun part!  Constraints specify positional requirements on particles, and the Constraint System works out the forces required so that these constraints are met.  There are five different kinds of Constraints in &progname;:
</para>

<formalpara>
<title>Circle:</title>Constrain a particle to travel in a circle. This means it must always be a fixed distance from a fixed x-y location, the centre of the circle.
</formalpara>

<formalpara>
<title>Distance:</title>Constrain two particles to be a set distance from each other.
</formalpara>

<formalpara>
<title>Horizontal / Vertical:</title>The particle can only travel in a horizontal or vertical plane.
</formalpara>

<para>
Fixed: Just a combination of the two above for convenience, the particle doesn't move anywhere.
</para>

</sect1>

<sect1 id="making_constraints">
<title> Making Constraints </title>


<figure><title>The Constraint Dialog</title>
<screenshot>
<mediaobject>
  <imageobject>
    <imagedata fileref="../images/constraint_dialog.png"/>
  </imageobject>
  <textobject><phrase>Screenshot of the Constraint Dialog</phrase></textobject>
</mediaobject>
</screenshot>
</figure>

<para>
To make a constraint, first add some particles to the simulation.  Then, open the Constraint Dialog box by choosing Make Constraint from the constraint menu.  When you select particles, certain options will become available in the box depending on how many particles you select.  You may also have to enter a number in the box before the constraint will be made.
</para>

<para>
Circle:
</para>
</sect1>
</chapter>

<chapter id="controlling_the_simulation">
<title>
Controlling the Simulation
</title>

<sect1 id="sim_toolbar">
<title>
The Sim Toolbar
</title>

<screenshot>
<graphic  fileref="../images/sim_toolbar.png" />
</screenshot>


<sect2 id="starting_and_stopping">
<title>
Starting and Stopping the Simulation
</title>
<para> Use the GO button </para>
</sect2>

<sect2 id="timestep">
<title>
The TimeStep - Delta t
</title>
<para>
This parameter is an input to the linear solver. It determines how far foward in time the solver steps each iteration. Larger values make the simulation run faster, but also make the simulation more inaccurate. At large values, the simulation can fail completely because the accumulated errors simply become too large. In short, changing this value will change the behaviour of the simulation.
</para>
</sect2>

<sect2 id="update_time">
<title>
Update Time
</title>
<para>
This is the time in milliseconds between simulation updates. Decreasing this value will increase the apparents speed of the simulations, but will not change its behaviour. This can be varied between 0 and 99 milliseconds.
</para>
</sect2>

<sect2 id="wall_bounce">
<title>
Wall Bounce
</title>
<para>
This parameter controls the amount of kinetic energy that collisions with the walls absorb from a particle, or how "bouncy" they appear. At 0.99 (the maximum value), the wall will return almost of the kinetic energy of the particle in the direction perpendicular to the wall. At 0, all of the kinetic energy in this direction will be absorbed by the wall.
</para>
</sect2>


<sect2 id="gravity">
<title>
Gravity
</title>
<para>
This parameter controls the strength of a downward force applied to all  particles in the simulation. It can be varied between 0 and 99.
</para>
</sect2>

<sect2 id="drag">
<title>
Viscous Drag
</title>
<para>
This paramater controls the strength of a "drag force" that also affects all particles. The force is always applied in the opposite direction of a particles motion. This acts to gradually slow the particles down. The force applied is proportional to the velocity of the particle. At high settings, it appears that the particles are moving through a thick liquid. Viscous Drag can be set to values between 0 and 0.99.
</para>
</sect2>

</sect1>

<sect1 id="the_linear_solvers">
<title>
The Linear Solvers
</title>
<para>
There are three different linear solvers implemented in springy sim. Each represents a different tradeoff between accuracy and computational effort.
</para>
<formalpara> <title>Euler Solver</title>
First order solver
</formalpara>
<formalpara> <title>Midpoint Method</title>
Second order solver
</formalpara>
<formalpara> <title>Runge-Kutta Solver</title>
Fourth order. This is the most accurate and should should be used when working with constraints.
</formalpara>
</sect1>
</chapter>

<chapter id="loading_and_saving">
<title>
Loading and Saving
</title>

<para> Foobar </para>

</chapter>

<chapter id="menu_items">
<title>
Menu Items
</title>

<sect1 id="file_menu">
<title>
File Menu
</title>
<para>
<figure><title>The File Menu</title>
<screenshot>
<mediaobject>
  <imageobject>
    <imagedata fileref="../images/file_menu.png"/>
  </imageobject>
  <textobject><phrase>Screenshot of the File Menu</phrase></textobject>
</mediaobject>
</screenshot>
</figure>
</para>
<itemizedlist>
<listitem>
<para>
New
</para>
</listitem>

<listitem>
<para>
Open
</para>
</listitem>

<listitem>
<para>
Save
</para>
</listitem>

<listitem>
<para>
Save As
</para>
</listitem>

<listitem>
<para>
Quit
</para>
</listitem>
</itemizedlist>

</sect1>

<sect1 id="edit_menu">
<title>
Edit Menu
</title>
<para> </para>

<itemizedlist id="edit_menu_list">

<listitem id="box_select_menu_item">
<para>
Box Select
</para>
</listitem>

<listitem id="make_springs_menu_item">
<para>
Make Springs
</para>
</listitem>

<listitem id="add_constraint_menu_item">
<para>
Add Constraint
</para>
</listitem>

<listitem id="snap_to_grid_menu_item">
<para>
Snap to Grid
</para>
</listitem>

<listitem id="set_grid_spacing_menu_item">
<para>
Set Grid Spacing
</para>
</listitem>

<listitem id="clear_all_menu_item">
<para>
Clear All
</para>
</listitem>

<listitem id="clear_springs_menu_item">
<para>
Clear Springs
</para>
</listitem>
</itemizedlist>

</sect1>

<sect1 id="view_menu">
<title>
View Menu
</title>
<para> </para>

<itemizedlist>
<listitem id="trace_particle_menu_item">
<para>
Trace Particle
</para>
</listitem>


<listitem id="clear_trace_menu_item">
<para>
Clear Trace
</para>
</listitem>

<listitem id="grid_menu_item">
<para>
Grid
</para>
</listitem>
</itemizedlist>

</sect1>

<sect1 id="sim_mode_menu">
<title>
Sim Mode Menu
</title>
<para> 
<figure><title>The Sim Mode Menu</title>
<screenshot>
<mediaobject>
  <imageobject>
    <imagedata fileref="../images/sim_mode_menu.png"/>
  </imageobject>
  <textobject><phrase>Screenshot of the Sim Mode Menu</phrase></textobject>
</mediaobject>
</screenshot>
</figure>
</para>

<itemizedlist>
<listitem id="constraint_mode_menu_item">
<para>
Constraint Mode
</para>
</listitem>

<listitem id="collisions_mode_menu_item">
<para>
Collisions Mode
</para>
</listitem>
</itemizedlist>

</sect1>

<sect1 id="solvers_menu">
<title>
Solvers Menu
</title>
<para> </para>

<itemizedlist>
<listitem>
<para>
Euler Solver
</para>
</listitem>

<listitem>
<para>
Midpoint Method
</para>
</listitem>

<listitem>
<para>
Runge Kutta 4
</para>
</listitem>

</itemizedlist>

</sect1>

<sect1 id="help_menu">
<title>
Help Menu
</title>
<para> </para>

<itemizedlist>
<listitem id="about_menu_item">
<para>
About
</para>
</listitem>

<listitem id="about_qt_menu_item">
<para>
About Qt
</para>
</listitem>

</itemizedlist>

</sect1>

</chapter>

<chapter id="physics">
<title>
Physics
</title>

<para> All the physics algorithms implemented in springysim are described in a series of papers by Andrew Witkin, David Baraff and Michael Kass. The papers are freely available <ulink url="http://www-2.cs.cmu.edu/afs/cs/user/baraff/www/pbm/pbm.html">here</ulink>.If these links become unavailable, Googling  "Introduction Physically Based Modeling" should turn up something.  Andrew Witkin and David Baraff also taught a course in 1994 at Carnegie-Mellon covering the same material. The course notes cover substantially the same material and are still availible <ulink url="http://www-2.cs.cmu.edu/afs/cs.cmu.edu/project/anim/aw/15-860/public/860home.html">here</ulink>. Many thanks to the authors for making these very useful documents freely available. In addition to the course notes, the course assigments are available. Completing these serves as a great introduction to these topics. </para>
<para>A certain level of mathematical ability is required to understand these papers, but not that much. First-year university mathematics, particularly matrix theory and basic differential equations should be adequate.</para>
<sect1 id="spring_algorithm">
<title>
Spring Algorithm
</title>
<para> </para>
</sect1>

<sect1 id="collisions_alg">
<title>
Collisions Algorithm
</title>
<para> </para>
</sect1>

<sect1 id="constraints_alg">
<title>
Constraints Algorithm
</title>
<para> </para>
</sect1>

<sect1 id="linear_solvers">
<title>
Linear Solvers
</title>
<para> </para>
</sect1>


</chapter>


<chapter id="implementation">
<title>
&progname; Implementation
</title>

<para> Foobar </para>

</chapter>


</book>
