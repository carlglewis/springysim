
%define version 0.1.0

Summary: A 2D interactive physics simulation with point masses, springs and rigid constraints.
Name: springysim
Version: %{version}
Release: 1.fc6
License: GPL
Group: Amusements/Graphics
Source: springysim-%{version}.tar.gz
BuildRoot: /var/tmp/%{name}-buildroot

%description
The eject program allows the user to eject removable media
(typically CD-ROMs, floppy disks or Iomega Jaz or Zip disks)
using software control. Eject can also control some multi-
disk CD changers and even some devices' auto-eject features.

Install eject if you'd like to eject removable media using
software control.

%prep
%setup -q
%configure

%build
make RPM_OPT_FLAGS="$RPM_OPT_FLAGS"

%install
rm -fr $RPM_BUILD_ROOT
%makeinstall 

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root)
%doc README COPYING ChangeLog AUTHORS INSTALL NEWS
%doc /usr/share/doc/springysim/*

/usr/share/springysim/resource/*
/usr/share/springysim/saves/*
/usr/bin/springysim

%changelog
* Tue Jun 12 2007 Carl G Lewis <carl.g.lewis@gmail.com> 
- First version of springysim specfile for FC6

